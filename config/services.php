<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'firebase' => [
        'api_key' => 'AIzaSyDKF8-gZPLARcvUTTP8I3ZZ6bTePwKDfqk',
        'auth_domain' => 'oslog-inventory.firebaseapp.com',
        'database_url' => '',
        'project_id' => 'oslog-inventory',
        'storage_bucket' => 'oslog-inventory.appspot.com',
        'messaging_sender_id' => '791278979165',
        'app_id' => '1:791278979165:web:165a64b7893adb8061e5b4',
        'measurement_id' => 'G-H44X1TRBHK',
    ],
];
