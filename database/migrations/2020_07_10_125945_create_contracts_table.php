<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->id();
            $table->string('new_filename');
            $table->string('old_filename');
            $table->string('file_extension');
            $table->string('file_size');
            $table->string('file_mime');
            $table->string('file_description');
            $table->string('client_id');
            $table->timestamp('signed_date')->nullable();
            $table->timestamp('renewal_date')->nullable();
            $table->integer('reminder');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}
