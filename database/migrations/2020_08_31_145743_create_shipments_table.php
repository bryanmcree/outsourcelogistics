<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipments', function (Blueprint $table) {
            $table->id();
            $table->string('record_type')->default('BC'); //This will always be BC
            $table->string('depositor_code')->nullable();  //Must match company name
            $table->string('order_ref_number')->nullable();
            $table->string('purchase_order_number')->nullable();
            $table->string('shipment_id_number')->nullable();
            $table->datetime('order_date')->nullable();
            $table->datetime('ship_date')->nullable();
            $table->string('ship_to_code')->nullable();
            $table->string('ship_to_name')->nullable();
            $table->string('ship_to_name_2')->nullable();
            $table->string('ship_to_address')->nullable();
            $table->string('ship_to_address_2')->nullable();
            $table->string('ship_to_city')->nullable();
            $table->string('ship_to_state')->nullable();
            $table->string('ship_to_zip')->nullable();
            $table->string('ship_to_country')->nullable();
            $table->string('scac')->nullable();
            $table->string('carrier_name')->nullable();
            $table->string('freight_bill_type')->nullable();
            $table->string('pro_number')->nullable();
            $table->string('hdr_user_1')->nullable();
            $table->string('hdr_user_2')->nullable();
            $table->string('hdr_user_3')->nullable();
            $table->string('item_number')->nullable();
            $table->string('qty_ordered')->nullable();
            $table->string('qty_shipped')->nullable();
            $table->string('uom')->nullable();
            $table->string('desc_1')->nullable();
            $table->string('desc_2')->nullable();
            $table->string('lot_number')->nullable();
            $table->string('line_reference')->nullable();
            $table->string('line_user_1')->default('0');
            $table->string('line_user_2')->default('0');
            $table->decimal('grs_weight', 8, 2)->default(0);
            $table->decimal('net_weight', 8, 2)->default(0);
            $table->string('warehouse')->nullable();
            $table->string('schedule_delivery')->nullable();
            $table->datetime('delivery_date')->nullable();
            $table->datetime('pickup_date')->nullable();
            $table->string('carrier')->nullable();
            $table->string('shipping_notes')->nullable();
            $table->string('batch_number')->nullable();
            $table->string('order_status')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipments');
    }
}
