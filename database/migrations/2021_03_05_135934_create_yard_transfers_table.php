<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateYardTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yard_transfers', function (Blueprint $table) {
            $table->id();

            $table->string('current_location_id')->nullable();  // name of the asset
            $table->string('new_location_id')->nullable();  // name of the asset
            $table->string('created_by')->nullable();  // name of the asset
            $table->string('detail_id')->nullable();  // name of the asset
            $table->string('priority')->nullable();  // name of the asset
            $table->string('move_status')->nullable();  // name of the asset

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yard_transfers');
    }
}
