<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpenseItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expense_items', function (Blueprint $table) {
            $table->id();
            $table->string('employee_id');
            $table->string('expense_id');
            $table->string('category');
            $table->string('file_name')->nullable();
            $table->string('extension')->nullable();
            $table->string('old_filename')->nullable();
            $table->string('new_filename')->nullable();
            $table->string('notes')->nullable();
            $table->string('vendor')->nullable();
            $table->string('approved')->nullable(); //0 for no 1 for yes
            $table->decimal('amount', 8, 2);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expense_items');
    }
}
