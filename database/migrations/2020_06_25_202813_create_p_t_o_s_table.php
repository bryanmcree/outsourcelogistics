<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePTOSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('p_t_o_s', function (Blueprint $table) {
            $table->id();
            $table->string('employee_id')->nullable();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->string('request_type')->nullable();
            $table->string('request_reason')->nullable();
            $table->string('supervisor_id')->nullable();
            $table->string('notes')->nullable();
            $table->string('request_status')->nullable();  //0
            $table->string('request_notes')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('p_t_o_s');
    }
}
