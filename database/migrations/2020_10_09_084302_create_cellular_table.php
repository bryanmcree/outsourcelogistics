<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCellularTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cellular', function (Blueprint $table) {
            $table->id();

            $table->string('user_name')->nullable();
            $table->string('mobile_no')->nullable();
            $table->string('account_no')->nullable();
            $table->string('device')->nullable();
            $table->string('device_type')->nullable();
            $table->dateTime('upgrade_date')->nullable();
            $table->string('current_offer')->nullable();
            $table->string('current_plan')->nullable();
            $table->string('category')->nullable();
            $table->string('sub_category')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cellular');
    }
}
