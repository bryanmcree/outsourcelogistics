<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToContainersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('containers', function (Blueprint $table) {
            $table->string('ch_number')->nullable();
            $table->string('desc_01')->nullable();
            $table->string('transportation_inv_number')->nullable();
            $table->string('desc_02')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('containers', function (Blueprint $table) {
            $table->dropColumn('ch_number')->nullable();
            $table->dropColumn('desc_01')->nullable();
            $table->dropColumn('transportation_inv_number')->nullable();
            $table->dropColumn('desc_02')->nullable();
        });
    }
}
