<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContainersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('containers', function (Blueprint $table) {
            $table->id();
            $table->string('customer')->nullable();
            $table->string('container_number')->nullable();
            $table->dateTime('estimated_arrival')->nullable(); //Vessel ETA
            $table->string('bl_number')->nullable();
            $table->string('dn_number')->nullable();
            $table->dateTime('estimated_lfd')->nullable();
            $table->dateTime('pickup_date')->nullable();
            $table->dateTime('drop_date')->nullable();
            $table->string('drop_location')->nullable();
            $table->dateTime('empty_date')->nullable();
            $table->dateTime('returned_date')->nullable();
            $table->dateTime('scan_date')->nullable();
            $table->dateTime('date_camelot')->nullable();
            $table->string('created_by')->nullable();
            $table->string('on_vessel')->nullable();
            $table->string('camelot_number')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('containers');
    }
}
