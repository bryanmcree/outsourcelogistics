<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->id();

            $table->string('name')->nullable();  // name of the asset
            $table->string('description')->nullable();  // description of the asset
            $table->string('asset_number')->nullable();  // number assigned to the asset
            $table->string('department')->nullable();  // department asset is assigned to
            $table->string('building')->nullable();  // building where the asset resides
            $table->string('location')->nullable();  // specific room or location in the building

            $table->dateTime('purchase_date')->nullable();  // date asset was purchased
            $table->string('vendor_id')->nullable();  // id of the vendor that provided this asset
            $table->float('purchase_price')->nullable();  // price of the asset when acquired
            $table->string('condition')->nullable();  // condition of the asset when acquired
            $table->float('unit_price')->nullable();  // unit price for the asset
            $table->integer('unit_qty')->nullable();  // number of assets

            $table->string('model_number')->nullable();
            $table->string('serial_number')->nullable();
            $table->string('service_tag')->nullable();
            $table->string('version')->nullable();
            $table->string('warranty_exp_date')->nullable();
            $table->string('category')->nullable();

            $table->string('mac_address')->nullable();
            $table->string('static_ip')->nullable();
            $table->string('computer_name')->nullable();

            $table->string('geo_tab_id')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('imei')->nullable();

            $table->string('notes')->nullable(); //Additional notes for the asset

            $table->string('created_by')->nullable(); //who entered the asset

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets');
    }
}
