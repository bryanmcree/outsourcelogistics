<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('title')->nullable();
            $table->string('building_number')->nullable();
            $table->string('site_location')->nullable();
            $table->string('work_ext')->nullable();
            $table->string('work_phone')->nullable();
            $table->string('fax_number')->nullable();
            $table->string('cell_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('title')->nullable();
            $table->dropColumn('building_number')->nullable();
            $table->dropColumn('site_location')->nullable();
            $table->dropColumn('work_ext')->nullable();
            $table->dropColumn('work_phone')->nullable();
            $table->dropColumn('fax_number')->nullable();
            $table->dropColumn('cell_number')->nullable();
        });
    }
}
