<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateYardTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yard_transaction', function (Blueprint $table) {
            $table->id();
            $table->string('location_id')->nullable();  // id of what yard and slot this is located
            $table->string('current_status')->nullable();  // what is the status of this


            $table->string('trailer_type')->nullable();  // trailer or container
            $table->string('trailer_number')->nullable();  // trailor or container number
            $table->string('loaded')->nullable();  // loaded or empty
            $table->string('driver_name')->nullable();  // driver name
            $table->string('driver_phone')->nullable();  // driver_phone
            $table->string('pickup_number')->nullable();  // pickup number
            $table->string('trailer_owner')->nullable();  // who owns the trailor
            $table->string('customer_name')->nullable();  // name of the customer
            $table->string('open_gate')->nullable();  // auto open gate

            $table->string('created_by')->nullable();  // Command Issued by
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yard_transaction');
    }
}
