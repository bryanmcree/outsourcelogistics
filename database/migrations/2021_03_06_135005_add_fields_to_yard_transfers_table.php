<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToYardTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('yard_transfers', function (Blueprint $table) {
            $table->string('assigned_to')->nullable();  // name of the asset
            $table->dateTime('completed_time')->nullable();  // name of the asset
            $table->dateTime('assignment_accepted_time')->nullable();  // name of the asset
            $table->dateTime('assignment_abandon_time')->nullable();  // name of the asset
            $table->string('abandoned_by')->nullable();  // name of the asset
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('yard_transfers', function (Blueprint $table) {
            $table->dropColumn('assigned_to');
            $table->dropColumn('completed_time');
            $table->dropColumn('assignment_accepted_time');
            $table->dropColumn('assignment_abandon_time');
            $table->dropColumn('abandoned_by');
        });
    }
}
