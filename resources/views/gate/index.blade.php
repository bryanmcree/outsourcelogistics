{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Gate Control') == FALSE)
        @include('layouts.unauthorized')

    @Else


        <div class="card mb-4">
            <div class="card-header text-white" style="background-color: #413c69;">
                <b>Outsource One Gate Control</b>
            </div>
            <div class="card-body">
                <p>
                    <a class="btn btn-secondary btn-lg btn-block" data-toggle="collapse" href="#yardEntrygate" role="button" aria-expanded="false" aria-controls="collapseExample">
                        <b>Yard Enter Gate</b>
                    </a>
                <div class="collapse mb-4" id="yardEntrygate">
                    <div class="card card-body">
                        <form method="post" action="/gate/driver">
                            @csrf
                            <input type="hidden" name="created_by" value="{{Auth::user()->id}}">
                            <input type="hidden" name="controller_id" value="1">
                            <div>
                                <input type="submit" value="Open Yard Enter Gate" name="controller_command" class="btn btn-lg btn-success btn-block">
                            </div>
                        </form>
                    </div>
                </div>
                    <a class="btn btn-secondary btn-lg btn-block" data-toggle="collapse" href="#yardExitGate" role="button" aria-expanded="false" aria-controls="collapseExample">
                        <b>Yard Exit Gate</b>
                    </a>
                </p>

                <div class="collapse mb-4" id="yardExitGate">
                    <div class="card card-body">
                        <form method="post" action="/gate/driver">
                            @csrf
                            <input type="hidden" name="created_by" value="{{Auth::user()->id}}">
                            <input type="hidden" name="controller_id" value="1">
                            <div>
                                <input type="submit" value="Open Yard Exit Gate" name="controller_command" class="btn btn-lg btn-success btn-block">
                            </div>
                        </form>
                    </div>
                </div>



                <a class="btn btn-secondary btn-lg btn-block" data-toggle="collapse" href="#employeeEntrygate" role="button" aria-expanded="false" aria-controls="collapseExample">
                    <b>Employee Entry Gate</b>
                </a>
                </p>

                <div class="collapse mb-4" id="employeeEntrygate">
                    <div class="card card-body">
                        <form method="post" action="/gate/driver">
                            @csrf
                            <input type="hidden" name="created_by" value="{{Auth::user()->id}}">
                            <input type="hidden" name="controller_id" value="1">
                            <div>
                                <input type="submit" value="Open Employee Entry Gate" name="controller_command" class="btn btn-lg btn-success btn-block">
                            </div>
                        </form>
                    </div>
                </div>


                <a class="btn btn-secondary btn-lg btn-block" data-toggle="collapse" href="#frontBayDoor" role="button" aria-expanded="false" aria-controls="collapseExample">
                    <b>Front Bay Door</b>
                </a>
                </p>

                <div class="collapse mb-4" id="frontBayDoor">
                    <div class="card card-body">
                        <form method="post" action="/gate/driver">
                            @csrf
                            <input type="hidden" name="created_by" value="{{Auth::user()->id}}">
                            <input type="hidden" name="controller_id" value="1">
                            <div>
                                <input type="submit" value="Open Front Bay Door" name="controller_command" class="btn btn-lg btn-success btn-block" disabled>
                                <input type="submit" value="Close Front Bay Door" name="controller_command" class="btn btn-lg btn-warning btn-block" disabled>
                                <input type="submit" value="Stop Front Bay Door" name="controller_command" class="btn btn-lg btn-danger btn-block" disabled>
                            </div>
                        </form>
                    </div>
                </div>


            <a class="btn btn-secondary btn-lg btn-block" data-toggle="collapse" href="#rearBayDoor" role="button" aria-expanded="false" aria-controls="collapseExample">
                <b>Rear Bay Door</b>
            </a>
            </p>

            <div class="collapse mb-4" id="rearBayDoor">
                <div class="card card-body">
                    <form method="post" action="/gate/driver">
                        @csrf
                        <input type="hidden" name="created_by" value="{{Auth::user()->id}}">
                        <input type="hidden" name="controller_id" value="1">
                        <div>
                            <input type="submit" value="Open Rear Bay Door" name="controller_command" class="btn btn-lg btn-success btn-block" disabled>
                            <input type="submit" value="Close Rear Bay Door" name="controller_command" class="btn btn-lg btn-warning btn-block" disabled>
                            <input type="submit" value="Stop Rear Bay Door" name="controller_command" class="btn btn-lg btn-danger btn-block" disabled>
                        </div>
                    </form>
                </div>
            </div>
            </div>
        </div>
    @endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection
