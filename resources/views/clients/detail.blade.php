{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Clients') == FALSE)
        @include('layouts.unauthorized')

    @Else


        <div class="card mb-4" style="background-color: #91bcff;">
            <div class="card-header text-white">
                <h2><b>{{$client->name}}</b></h2>
            </div>
            <div class="card-body">
                <div class="card mb-3">
                    Placeholder for customer data
                </div>
                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active btn-sm" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false"><i class="fad fa-file-contract"></i> Contracts</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Charges</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">...</div>
                            <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">

                                @if($contracts->isEmpty())
                                    <div class="alert alert-info">No Contracts <a href="#" class="btn btn-sm btn-primary float-right" data-toggle="modal" data-target="#exampleModalCenter"><i class="fad fa-file-contract"></i> New Contract</a></div>
                                @else
                                <table class="table table-sm table-hover">
                                    <thead>
                                        <tr>
                                            <td><b>Contract</b></td>
                                            <td><b>Signed</b></td>
                                            <td><b>Signed Date</b></td>
                                            <td><b>Renewal Date</b></td>
                                            <td><b>Reminder</b></td>
                                            <td align="right"><a href="#" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#exampleModalCenter"><i class="fad fa-file-contract"></i> New Contract</a> </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($contracts as $contract)
                                        <tr>
                                            <td>{{$contract->old_filename}}</td>
                                            <td><i class="fad fa-check-square fa-lg"></i></td>
                                            <td>{{$contract->signed_date->format('m-d-Y')}}</td>
                                            <td>{{$contract->renewal_date->format('m-d-Y')}}</td>
                                            <td>{{$contract->reminder}}</td>
                                            <td></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                @endif
                            </div>
                            <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                                Charges
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@include('clients.modal.add_contract')
@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection
