<style>

    body {
        display: flex;
        text-align: center;
    }

    @page {
        size: A4 landscape;
    }


</style>


<body>
<table width="100%" border="1" cellpadding="3" cellspacing="3">
    <tr>
        <td align="center"><span style="font-size: 150px; font-weight: bold">{{strtoupper($barcode)}}</span></td>
    </tr>
    <tr>
        <td align="center"><span style="font-size: 600px; font-family: 'BC C39 3 to 1 HD Medium'">*{{strtoupper($barcode)}}*</span></td>
    </tr>
</table>

</body>
