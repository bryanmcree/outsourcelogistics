{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')




        <div class="card mb-4">
            <div class="card-header text-white" style="background-color: #413c69;">
                <b>Got Ya!</b>
            </div>
            <div class="card-title">
                <i>Get your singing voice ready...</i>
            </div>
            <div class="card-body">
                <div class="card-deck">
                    <div class="card">
                        <div class="card-header text-center">
                            <b>BRYAN</b>
                        </div>
                        <div class="card-body text-center" style="font-size: 35px; font-weight: bold;">
                            1
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header text-center">
                            <b>SAVANNAH</b>
                        </div>
                        <div class="card-body text-center" style="font-size: 35px; font-weight: bold;">
                            0
                        </div>
                    </div>
                </div>
            </div>
        </div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection
