{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.portal')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')


@if(!$pendingShipments->isEmpty())
    <div class="alert alert-warning">You have pending shipments to submit.</div>
@endif

    <div class="card mb-4">
        <div class="card-header" style="background-color: #7bc26c">
            <b>Current Inventory</b>
        </div>
        <div class="card-body">
            <table class="table table-sm table-hover" id="items">
                <thead>
                <tr>
                    <td><b>Item</b></td>
                    <td><b>Description</b></td>
                    <td><b>UOS</b></td>
                    <td><b>Receipt</b></td>
                    <td><b>Qty</b></td>
                    <td><b>Warehouse</b></td>
                    <td><b>Last Updated</b></td>
                    <td align="right"></td>
                </tr>
                </thead>
                <tbody>
                @foreach($items as $item)
                    <tr>
                        <td>{{$item->item}}</td>
                        <td>{{$item->description}}</td>
                        <td>{{$item->unit}}</td>
                        <td>{{$item->orig_document}}</td>
                        <td>{{$item->qty_remain}}</td>
                        <td>{{$item->warehouse}}</td>
                        <td align="">{{$item->last_updated}}</td>
                        <td align="right"><a href="/portal/ship/{{$item->item_id}}" class="btn btn-sm btn-success"><i class="fad fa-shipping-fast"></i> Ship</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#items').DataTable();
        } );

    </script>

@endsection
