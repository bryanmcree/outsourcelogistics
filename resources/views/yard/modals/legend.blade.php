<!-- Modal -->
<div class="modal fade" id="YardLegend" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header text-white"  style="background-color: #1a32ba;">
                <h5 class="modal-title" id="exampleModalLabel"><b>Yard Legend</b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row mb-2">
                    <div class="col-2 bg-danger"></div>
                    <div class="col-10">This location is damaged and not available.</div>
                </div>
                <div class="row mb-2">
                    <div class="col-2 bg-success"></div>
                    <div class="col-10">This location is empty and is available.</div>
                </div>
                <div class="row mb-2">
                    <div class="col-2 bg-info"></div>
                    <div class="col-10">This location has equipment installed and is not available.</div>
                </div>
                <div class="row mb-2">
                    <div class="col-2 bg-warning"></div>
                    <div class="col-10">This location is reserved for a pending transfer and is not available.</div>
                </div>
                <div class="row mb-2">
                    <div class="col-2 bg-dark"></div>
                    <div class="col-10">This location is occupied and not available.</div>
                </div>
                <div class="row mb-2">
                    <div class="col-2 text-center"><i class="fad fa-truck"></i></div>
                    <div class="col-10">Trailer at location</div>
                </div>
                <div class="row mb-2">
                    <div class="col-2 text-center"><i class="fad fa-container-storage"></i></div>
                    <div class="col-10">Container at location</div>
                </div>
                <div class="row mb-2">
                    <div class="col-2 text-center"><i class="fad fa-trailer"></i></div>
                    <div class="col-10">Empty chassis at location</div>
                </div>
                <div class="row mb-2">
                    <div class="col-2 text-center"><i class="fad fa-truck-pickup"></i></div>
                    <div class="col-10">Truck at this location</div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
