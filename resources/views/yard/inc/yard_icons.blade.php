@if($dock->location_status == 'Empty')
@elseif($dock->location_status == 'Damaged') <i class="fad fa-do-not-enter" title="Unavailable"></i>
@elseif($dock->location_status == 'Equipment') <i class="fad fa-do-not-enter" title="Unavailable"></i>
@elseif($dock->location_status == 'Transfer') <i class="fad fa-exchange" title="Transfer in process"></i>
@elseif($dock->trailer_type == 'Running Refer') <i class="fad fa-refrigerator" title="Running Refer Unit"></i>
@elseif($dock->trailer_type == 'Container') <i class="fad fa-container-storage" title="Container"></i>
@elseif($dock->trailer_type == 'Trailer') <i class="fad fa-trailer" title="Trailer"></i>
@elseif($dock->trailer_type == 'Other') <i class="fad fa-question-square" title="Other"></i>
@endif
