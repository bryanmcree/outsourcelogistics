{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.yard')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Yard') == FALSE)
        @include('layouts.unauthorized')

    @Else

            <div class="col-sm-12">
                <div class="card bg-dark border-dark mb-0">
                    <div class="card-header bg-outline">

                        <div class="col-sm-12">
                            <form method="post" action="/yard/search">
                                @csrf
                                <div class="input-group input-group-sm mb-3">
                                    <div class="input-group-prepend">
                                        <button class="btn-outline-dark text-white">OSL 1</button>
                                        <a href="#" class="btn btn-sm btn-info disabled" data-toggle="modal" data-target="#YardLegend"><i class="fad fa-map-marker-alt"></i> Change Locations</a>
                                        <a href="#" class="btn btn-sm btn-info disabled" data-toggle="modal" data-target="#YardLegend"><i class="fad fa-exchange"></i> Pending Transfers</a>
                                        <a href="#" class="btn btn-sm btn-info" data-toggle="modal" data-target="#GateControl"><i class="fad fa-map-signs"></i> Gate Control</a>
                                        <a href="#" class="btn btn-sm btn-info" data-toggle="modal" data-target="#YardLegend"><i class="fad fa-map-signs"></i> Legend</a>
                                    </div>
                                    <input type="text" name="yardsearch" class="form-control" placeholder="" aria-label="" aria-describedby="basic-addon1">
                                    <div class="input-group-append">
                                        <input type="submit" class="btn btn-sm btn-secondary" value="Search">
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                    <div class="card-body">
                        <!-- Ajax loads data below -->
                        <div class="" id="LoadYard"></div>
                        <!-- Ajax Done -->
                    </div>
                </div>
            </div>

    @endif

    @include("yard.modals.legend")
    @include("yard.modals.locationDetail")
    @include("yard.modals.gates")
@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        //Auto Update
        $(document).ready(function() {
            $('#LoadYard').load('/dock/feed');
            var refreshId2 = setInterval(function() {
                $("#LoadYard").load('/dock/feed');
                //alert('reloaded')
            }, 1000);
            $.ajaxSetup({ cache: false });
        });
        //END Auto Update


        $(function(){
            $(document).on("click", ".LocationDetail", function () {
                //alert('got your click!');
                var item_id = $(this).attr("data-location_id");
                //alert(item_id);
                $('.location_id').val($(this).attr("data-location_id")); //Add to hidden location ID field
                $.ajax({
                    type:'get',
                    url: '/yard/detail/' + item_id,
                    data: {
                        '_token': $('input[name=_token]').val()
                    },
                    success:function(data){
                        if(data && data.length){
                            $('#location_detail').modal('show');
                            for(var i = 0; i < data.length;i++){  //Loop through the Json

                                if(data[i].location_status === "Empty") {
                                    $("#Ingate").show();
                                    $("#Outgate").hide();
                                    $("#RequestTransfer").hide();
                                    $("#Unloaded").hide(); //unloaded option
                                }else if(data[i].location_status === "Complete"){
                                    $("#Ingate").hide();
                                    $("#Outgate").show();
                                    $("#RequestTransfer").show();
                                    $("#Unloaded").hide(); //unloaded option
                                }else if(data[i].location_status === "Occupied"){
                                    $("#Ingate").hide();
                                    $("#Outgate").show();
                                    $("#RequestTransfer").show();
                                    $("#Unloaded").show(); //unloaded option
                                }else if(data[i].location_status === "Equipment"){
                                    $("#Ingate").hide();
                                    $("#Outgate").hide();
                                    $("#RequestTransfer").hide();
                                    $("#Unloaded").hide(); //unloaded option
                                }else if(data[i].location_status === "Damaged"){
                                    $("#Ingate").hide();
                                    $("#Outgate").hide();
                                    $("#RequestTransfer").hide();
                                    $("#Unloaded").hide(); //unloaded option
                                }

                                if(data[i].location_status === "Damaged" ||
                                    data[i].location_status === "Equipment" ||
                                    data[i].location_status === "Transfer")
                                {
                                    $("#Ingate").hide();
                                    $("#Outgate").hide();
                                    $("#RequestTransfer").hide();
                                    $("#Unloaded").hide();
                                }

                                var Locationid = $('.Location');
                                var location_id = "<div class=''><b>" + data[i].location_type + " - "+ data[i].location_number +" (" + data[i].location_status +") </b></div>"
                                Locationid.html(location_id);

                                var ReceiveButton = $('.RecButton');
                                var Receive_Button = "<input type=submit class='btn btn-block btn-primary' value= 'Receive at " + data[i].location_type +" "+ data[i].location_number +"' >"
                                ReceiveButton.html(Receive_Button);

                                var OutGateButton = $('.OGButton');
                                var OutGate_Button = "<input type=submit class='btn btn-block btn-danger' value= 'Out Gate " + data[i].location_type +" "+ data[i].location_number +"' >"
                                OutGateButton.html(OutGate_Button);

                                //var trailerNumber = data[i].yard_trans.driver_name;
                                console.log(data[i].yard_trans.trailer_type)
                                $('.trailer_number').val(data[i].yard_trans.trailer_number);
                                $('.driver_name').val(data[i].yard_trans.driver_name);
                                $('.driver_phone').val(data[i].yard_trans.driver_phone);
                                $('.trailer_owner').val(data[i].yard_trans.trailer_owner);
                                $('.customer_name').val(data[i].yard_trans.customer_name);
                                $('.trailer_type').val(data[i].yard_trans.trailer_type);
                                $('.pickup_number').val(data[i].yard_trans.pickup_number);
                                $('.loaded').val(data[i].yard_trans.loaded);


                            }
                        }else{
                            console.log('no related items found');
                        }
                    }
                });

            });
        });

    </script>

@endsection
