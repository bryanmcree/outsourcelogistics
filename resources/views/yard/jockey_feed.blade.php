
@if(!is_null($alreadyAsisgned))
You already got stuff assigned

@else
    @if($transfer->isEmpty())

        <div class="alert alert-info">No pending move request.</div>
    @else
        <h6 class="card-subtitle mb-2 text-muted">{{$transfer->count()}} Pending Request</h6>
        @foreach($transfer as $move)

            <div class="card mb-2 @if($move->priority =='High') border-danger @else border-warning @endif">
                <div class="card-header @if($move->priority =='High') bg-danger text-white @else bg-warning @endif">
                    <b>Transfer Request</b> - {{$move->priority}} - Bld: {{$move->CurrentLOC->location_bld}}
                </div>
                <div class="card-body" >
                    <div class="row">
                        <div class="col-sm-3 mb-2">
                            <div class="card">
                                <div class="card-header bg-secondary">
                                    <b>Details</b>
                                </div>
                                <div class="card-body">
                                    <b>Type:</b> {{$move->TransDetail->trailer_type}} - {{$move->TransDetail->trailer_number}} - {{$move->TransDetail->loaded}}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 mb-2">
                            <div class="card">
                                <div class="card-header bg-secondary">
                                    <b>From</b>
                                </div>
                                <div class="card-body">
                                    Bld: {{$move->CurrentLOC->location_bld}} - {{$move->CurrentLOC->location_type}} - {{$move->CurrentLOC->location_number}}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 mb-2">
                            <div class="card">
                                <div class="card-header bg-secondary">
                                    <b>To</b>
                                </div>
                                <div class="card-body">
                                    Bld: {{$move->NewLOC->location_bld}} - {{$move->NewLOC->location_type}} - {{$move->NewLOC->location_number}}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 mb-2">
                            <div class="card">
                                <div class="card-header bg-secondary">
                                    <b>Time and Who</b>
                                </div>
                                <div class="card-body">
                                    {{ \Carbon\Carbon::parse($move->created_at)->diffForhumans() }} by {{$move->createdBy->first_name}} {{$move->createdBy->last_name}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <a href="/yard/switch/assign/{{$move->id}}" class="btn btn-block btn-lg @if($move->priority =='High') btn-danger @else btn-warning @endif">Accept This Assignment</a>
                </div>
            </div>
        @endforeach
    @endif

@endif

