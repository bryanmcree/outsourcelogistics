{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.yard')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Yard') == FALSE)
        @include('layouts.unauthorized')

    @Else


        <div class="card mb-1 mt-2">
            <div class="card-header text-white" style="background-color: #413c69;">
                <b>Current Assignment</b>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><a href="/yard/switch/complete/{{$assignment->id}}" class="btn btn-large btn-success btn-block"><b>Mark This Move Complete</b></a> </li>
                <li class="list-group-item"><b>From: </b>Bld:{{$assignment->CurrentLOC->location_bld}} - {{$assignment->CurrentLOC->location_type}} - {{$assignment->CurrentLOC->location_number}}</li>
                <li class="list-group-item"><b>TO:</b> Bld: {{$assignment->NewLOC->location_bld}} - {{$assignment->NewLOC->location_type}} - {{$assignment->NewLOC->location_number}}</li>
                <li class="list-group-item"><b>Requested by:</b> {{ \Carbon\Carbon::parse($assignment->created_at)->diffForhumans() }} by {{$assignment->createdBy->first_name}} {{$assignment->createdBy->last_name}}</li>
                <li class="list-group-item"><b>Time Requested:</b> {{$assignment->created_at}} ({{ \Carbon\Carbon::parse($assignment->created_at)->diffForhumans() }})</li>
                <li class="list-group-item"><b>Assigned to:</b> {{$assignment->AssignedTo->first_name}} {{$assignment->AssignedTo->last_name}}</li>
                <li class="list-group-item"><b>Trailer Type:</b> {{$assignment->TransDetail->trailer_type}}</li>
                <li class="list-group-item"><b>Number:</b> {{$assignment->TransDetail->trailer_number}}</li>
                <li class="list-group-item"><b>Current Status:</b> {{$assignment->TransDetail->current_status}}</li>
                <li class="list-group-item"><b>Move Status:</b> {{$assignment->move_status}}</li>
                <li class="list-group-item"><a href="/yard/switch/abandon/{{$assignment->id}}" class="btn btn-sm btn-danger btn-block">Abandon This Assignment</a></li>
            </ul>
        </div>
    @endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection
