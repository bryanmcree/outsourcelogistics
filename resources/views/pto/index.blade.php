{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')




        <div class="card mb-4">
            <div class="card-header" style="background-color: #91bcff;">
                <b>PTO Dashboard</b>
            </div>
            <div class="card-body">
                @if(!$pendingPTO->isEmpty())
                <div class="card mb-3">
                    <div class="card-header bg-warning"><b>Pending PTO Request</b></div>
                    <div class="card-body">
                        <table class="table table-hover table-sm">
                            <thead>
                                <tr>
                                    <td><b>Employee Name</b></td>
                                    <td><b>Start Date</b></td>
                                    <td><b>End Date</b></td>
                                    <td><b>Time</b></td>
                                    <td><b>Type</b></td>
                                    <td><b>Reason</b></td>
                                    <td><b>Request Date</b></td>
                                    <td><b>Notes</b></td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($pendingPTO as $pending)
                                <tr>
                                    <td>{{$pending->employee->first_name}} {{$pending->employee->last_name}}</td>
                                    <td>{{$pending->start_date->format('m-d-y h:s')}}</td>
                                    <td>{{$pending->end_date->format('m-d-y h:s')}}</td>
                                    <td>
                                        @if(\Carbon\Carbon::parse($pending->start_date)->diffInHours($pending->end_date) > 12)

                                            {{\Carbon\Carbon::parse($pending->start_date)->diffInWeekdays($pending->end_date)}} Days

                                        @else
                                            {{\Carbon\Carbon::parse($pending->start_date)->diffInHours($pending->end_date)}} Hours
                                        @endif
                                    </td>
                                    <td>
                                        @if($pending->request_type == 'WITH Pay')
                                            <i class="fad fa-money-bill-alt" title="With Pay"></i>

                                        @else
                                            <span class="fa-stack fa-1x" title="Without Pay">
                                              <i class="fad fa-money-bill-alt"></i>
                                              <i class="fas fa-ban fa-stack-1x" style="color:Tomato; left: -10px;"></i>
                                            </span>
                                        @endif
                                    </td>
                                    <td>{{$pending->request_reason}}</td>
                                    <td>{{$pending->created_at->format('m-d-Y')}}</td>
                                    <td>{{$pending->notes}}</td>
                                    <td align="right">[Button]</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                @endif
                <div class="card-deck">
                    <div class="card">
                        <div class="card-header"style="background-color: #91bcff;"><b>Current PTO Request</b></div>
                        <div class="card-body">
                            @if($ptoRequest->isEmpty())
                            <div class="alert alert-info">You don't have any time off request.</div>
                            @else
                            <table class="table table-hover table-sm">
                                <tbody>
                                @foreach($ptoRequest as $pto)
                                    <tr>
                                        <td>{{$pto->start_date->format('m-d-y h:s')}}</td>
                                        <td>{{$pto->end_date->format('m-d-y h:s')}}</td>
                                        <td>

                                            @if(\Carbon\Carbon::parse($pto->start_date)->diffInHours($pto->end_date) > 12)

                                            {{\Carbon\Carbon::parse($pto->start_date)->diffInWeekdays($pto->end_date)}} Days

                                            @else
                                                {{\Carbon\Carbon::parse($pto->start_date)->diffInHours($pto->end_date)}} Hours
                                            @endif

                                        </td>
                                        <td>{{$pto->request_reason}}</td>
                                        <td>
                                            @if($pto->request_type == 'WITH Pay')
                                            <i class="fad fa-money-bill-alt" title="With Pay"></i>

                                            @else
                                            <span class="fa-stack fa-1x" title="Without Pay">
                                              <i class="fad fa-money-bill-alt"></i>
                                              <i class="fas fa-ban fa-stack-1x" style="color:Tomato; left: -10px;"></i>
                                            </span>
                                                @endif
                                        </td>
                                        <td>
                                            @if($pto->request_status == 0)
                                            Pending
                                            @elseif($pto->request_status == 1)
                                            Approved
                                            @else
                                            Denied
                                            @endif
                                        </td>
                                        <td align="right">
                                            <a href="#" class="btn btn-sm btn-secondary" title="View Request"><i class="fad fa-eye"></i></a>
                                            <a href="#" class="btn btn-sm btn-primary" title="Edit Request (Requires Re-approval)"><i class="fad fa-edit"></i></a>
                                            <a href="/pto/delete/{{$pto->id}}" class="btn btn-sm btn-danger" title="Delete Request"><i class="fad fa-trash-alt"></i></a>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            @endif
                        </div>
                    </div>
                    <div class="card">
                        <form method="post" action="/pto/add">
                            @csrf
                            <input type="hidden" name="employee_id" value="{{Auth::user()->id}}">
                            <input type="hidden" name="request_status" value="0">
                            <div class="card-header" style="background-color: #91bcff;"><b>New Request</b></div>
                            <div class="card-body">
                                <form>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1"><b>Start Date and Time</b></label>
                                                <input type="datetime-local" name="start_date" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                                                <small id="emailHelp" class="form-text text-muted">Select the start date and time of when you want off.</small>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1"><b>End Date and Time</b></label>
                                                <input type="datetime-local" name="end_date" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                                                <small id="emailHelp" class="form-text text-muted">Select the start date and time of when you want off.</small>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1"><b>Request Type</b></label>
                                                <select class="form-control" name="request_type" id="exampleFormControlSelect1">
                                                    <option value="" selected>[Select One]</option>
                                                    <option value="WITH Pay">WITH Pay</option>
                                                    <option value="WITHOUT Pay">WITHOUT Pay</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1"><b>Request Reason</b></label>
                                                <select class="form-control" name="request_reason" id="exampleFormControlSelect1">
                                                    <option selected>[Select One]</option>
                                                    <option value="Vacation">Vacation</option>
                                                    <option value="Personal Leave">Personal Leave</option>
                                                    <option value="Funeral / Bereavement">Funeral / Bereavement</option>
                                                    <option value="Jury Duty">Jury Duty</option>
                                                    <option value="Family Reasons">Family Reasons</option>
                                                    <option value="Medical Leave">Medical Leave</option>
                                                    <option value="To Vote">To Vote</option>
                                                    <option value="Other">Other (Include in Notes)</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1"><b>Select Supervisor</b></label>
                                                <select class="form-control" name="Supervisor_id" id="exampleFormControlSelect1">
                                                    <option selected>[Select Supervisor]</option>
                                                    @foreach($allUsers as $user)
                                                        <option value="{{$user->id}}">{{$user->last_name}}, {{$user->first_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="exampleFormControlTextarea1"><b>Notes</b></label>
                                                <textarea class="form-control" name="notes" id="exampleFormControlTextarea1" rows="1"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="submit" class="btn btn-sm btn-primary" name="" value="Request Time Off">
                                    <input type="reset" class="btn btn-sm btn-secondary float-right" value="Clear Form">
                                </form>
                            </div>
                        </form>

                    </div>
                </div>
                @if ( Auth::user()->hasRole('PTO Admin'))
                <div class="card mt-4">
                    <div class="card-header">
                        <b>Admin View</b>
                    </div>
                    <div class="card-body">
                        Maybe a calendar view help supervisors see who is off and when?
                    </div>
                @endif
                </div>
            </div>
        </div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection
