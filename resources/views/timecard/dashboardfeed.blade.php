<div class="card-deck mb-2">
    <div class="card border-dark">
        <div class="card-header">
            <b>Clocked In Employees</b> ({{$clockedin->count()}})
        </div>
        <div class="card-body" style="max-height: 400px; overflow-y: auto;">
            @if($clockedin->isEmpty())
                <div class="alert alert-info">No clocked in employees</div>
            @else
                <table class="table table-sm table-hover" id="clocked_in">
                    <thead>
                    <tr>
                        <td><b>Name</b></td>
                        <td><b>Building</b></td>
                        <td><b>Time</b></td>
                        <td><b>Type</b></td>
                        <td><b>IP</b></td>
                        <td><b>Loc</b></td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($clockedin as $in)
                        <tr>
                            <td>{{$in->employee->first_name}} {{$in->employee->last_name}}</td>
                            <td>{{$in->building_location}}</td>
                            <td>{{$in->in_time->format('h:i A')}}</td>
                            <td>{{$in->punch_type}}</td>
                            <td>{{$in->ip_address}}</td>
                            <td align="center"><i class="fas fa-globe-americas mapDetail"
                                   data-clockIn_long="{{$in->long}}"
                                   data-clockIn_lat="{{$in->lat}}"
                                   data-clockOut_long="{{$in->long_out}}"
                                   data-clockOut_lat="{{$in->lat_out}}"


                                ></i></td>
                            <td align="right"><a href="#" class="btn btn-sm btn-dark punchDetail"

                                                 data-id = '{{$in->id}}'
                                                 data-building_location = '{{$in->building_location}}'
                                                 data-in_time = '{{$in->in_time->format('Y-m-d\TH:i')}}'
                                                 data-out_time = '@if(is_null($in->out_time)) @else {{$in->out_time->format('Y-m-d\TH:i')}} @endif'
                                                 data-notes = '{{$in->notes}}'

                                ><i class="fad fa-stopwatch"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
    <div class="card border-dark">
        <div class="card-header">
            <b>Daily Transactions</b>
            <div class="btn-group-sm btn-group float-right">
                <input type="date" name="" class="form-control form-control-sm">
                <button class="form-control form-control-sm">GO</button>
            </div>
        </div>
        <div class="card-body"  style="max-height: 400px; overflow-y: auto;">
            @if($transactions->isEmpty())
                <div class="alert alert-info">No transactions for today.</div>
            @else
                <table class="table table-sm table-hover" id="transactions">
                    <thead>
                    <tr>
                        <td><b>Name</b></td>
                        <td><b>Bld</b></td>
                        <td><b>In</b></td>
                        <td><b>Out</b></td>
                        <td><b>Min</b></td>
                        <td><b>Time</b></td>
                        <td><b>Loc</b></td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($transactions as $tran)
                        <tr>
                            <td>{{$tran->employee->first_name}} {{$tran->employee->last_name}}</td>
                            <td>{{$tran->building_location}}</td>
                            <td>@if(is_null($tran->in_time)) @else {{$tran->in_time->format('h:i A')}} @endif</td>
                            <td>@if(is_null($tran->out_time)) @else {{$tran->out_time->format('h:i A')}} @endif</td>
                            <td align="center">{{$tran->total_min}}</td>
                            <td align="center">{{$tran->total_time}}</td>
                            <td align="center"><i class="fas fa-globe-americas mapDetail"
                                                  data-clockIn_long="{{$tran->long}}"
                                                  data-clockIn_lat="{{$tran->lat}}"
                                                  data-clockOut_long="{{$tran->long_out}}"
                                                  data-clockOut_lat="{{$tran->lat_out}}"


                                ></i></td>
                            <td align="right"><a href="#" class="btn btn-sm btn-dark punchDetail"

                                                 data-id = '{{$tran->id}}'
                                                 data-building_location = '{{$tran->building_location}}'
                                                 data-in_time = '{{$tran->in_time->format('Y-m-d\TH:i')}}'
                                                 data-out_time = '@if(is_null($tran->out_time)) @else{{$tran->out_time->format('Y-m-d\TH:i')}}@endif'
                                                 data-notes = '{{$tran->notes}}'
                                                 data-total_min = '{{$tran->total_min}}'
                                                 data-total_time = '{{$tran->total_time}}'

                                ><i class="fad fa-stopwatch"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td  align="right"><b>Total</b></td>
                        <td align="center"><b>{{$transactions->sum('total_min')}}</b></td>
                        <td align="center"><b>{{$transactions->sum('total_time')}}</b></td>
                        <td></td>
                        <td></td>
                    </tr>
                    </tfoot>
                </table>
            @endif
        </div>
    </div>
</div>
<div class="card-deck">
    <div class="card border-dark">
        <div class="card-header bg-warning">
            <b>Miss Punches</b>
        </div>
        <div class="card-body">
            @if($mps->isEmpty())
                <div class="alert alert-info">No Miss Punches</div>
            @else
                <table class="table table-sm table-hover" id="mp">
                    <thead>
                    <tr>
                        <td><b>Name</b></td>
                        <td><b>Building</b></td>
                        <td><b>In Time</b></td>
                        <td><b>Out Time</b></td>
                        <td><b>ToT Time</b></td>
                        <td><b>ToT Min</b></td>
                        <td><b>LT</b></td>
                        <td><b>Msg</b></td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($mps as $mp)
                        <tr class="table-danger">
                            <td>{{$mp->employee->first_name}} {{$mp->employee->last_name}}</td>
                            <td>{{$mp->building_location}}</td>
                            <td>{{$mp->in_time->format('m-d-y h:i A')}}</td>
                            <td>@if(is_null($mp->out_time)) -- @else {{$mp->out_time->format('m-d-y h:i A')}}@endif</td>
                            <td>{{$mp->total_time}}</td>
                            <td>{{$mp->total_min}}</td>
                            <td>{{$mp->less_lunch_min}}</td>
                            <td>
                                @if(is_null($mp->out_time))
                                    No Out Time
                                @elseif($mp->total_time > 8)
                                    Did not clock out for lunch
                                @endif

                            </td>
                            <td align="right"><a href="#" class="btn btn-sm btn-dark punchDetail"

                                                 data-id = '{{$mp->id}}'
                                                 data-building_location = '{{$mp->building_location}}'
                                                 data-in_time = '{{$mp->in_time->format('Y-m-d\TH:i')}}'
                                                 data-out_time = '@if(is_null($mp->out_time)) @else{{$mp->out_time->format('Y-m-d\TH:i')}}@endif'
                                                 data-miss_punch_by = '{{$mp->notes}}'
                                                 data-notes = '{{$mp->notes}}'

                                                 data-total_min = '{{$mp->total_min}}'
                                                 data-total_time = '{{$mp->total_time}}'
                                                 data-less_lunch_min = '{{$mp->less_lunch_min}}'

                                ><i class="fad fa-stopwatch"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myMapModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header text-white" style="background-color: #1a32ba;">
                <h5 class="modal-title text-white" id="exampleModalLongTitle">Clocking Map</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="googleMap" style="width:100%;height:600px;"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>




<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAlapyIgcGKgH0lX89IN1UXGTEK37ZZuCs&callback=myMap"></script>

<script>
    $(document).ready( function () {
        $('#clocked_in').DataTable({
            "paging": false
        });
    } );

    $(document).ready( function () {
        $('#transactions').DataTable({
            "paging": false
        });
    } );

    $(document).ready( function () {
        $('#mp').DataTable({
            "paging": false
        });
    } );

    //Map Detail
    $(document).on("click", ".mapDetail", function (e) {
        var clockIn_long = $(this).attr("data-clockIn_long")
        var clockIn_lat = $(this).attr("data-clockIn_lat")
        var clockOut_long = $(this).attr("data-clockOut_long")
        var clockOut_lat = $(this).attr("data-clockOut_lat")
        myMap(clockIn_long, clockIn_lat, clockOut_long, clockOut_lat);
        $('#myMapModal').modal('show');
    });
    //END Map Detail

    function myMap(clockIn_long, clockIn_lat, clockOut_long, clockOut_lat) {
        //alert(clockOut_lat);
        const clockedIn = { lat: parseFloat(clockIn_lat), lng: parseFloat(clockIn_long) };
        const clockedOut = { lat: parseFloat(clockOut_lat), lng: parseFloat(clockOut_long) };

        const map = new google.maps.Map(document.getElementById("googleMap"), {
            zoom: 14,
            center: clockedIn,
        });
        new google.maps.Marker({
            position: clockedIn,
            map,
            label: {
                fontFamily: 'Fontawesome',
                text: '\uf2f6'
            },
            clickable: false,
            title: "Clocked In",
        });
        new google.maps.Marker({
            position: clockedOut,
            map,
            label: {
                fontFamily: 'Fontawesome',
                text: '\uf2f5'
            },
            title: "Clocked In",
        });
    }



</script>
