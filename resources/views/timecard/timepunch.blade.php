{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.timeclock')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')


    <?php


    // 24-hour format of an hour without leading zeros (0 through 23)
    $Hour = date('G');

    if ( $Hour >= 5 && $Hour <= 11 ) {
        $greeting = "Good Morning";
    } else if ( $Hour >= 12 && $Hour <= 18 ) {
        $greeting = "Good Afternoon";
    } else if ( $Hour >= 19 || $Hour <= 4 ) {
        $greeting = "Good Evening";
    }
    ?>

    <div class="col-lg-3 offset-lg-4">
        <div class="card mt-5">
            <div class="card-header text-center">
                <img src="{{url('img/Outsource Logistics_Transparent_300.png')}}" class="">
            </div>
            <div class="card-body">
                <form method="post" action="/timeclock/punch">
                    @csrf
                    <input type="hidden" id="long" name="long">
                    <input type="hidden" id="lat" name="lat">
                    <input type="hidden" value="{{$timeclockuser->id}}" name="id">
                    <div class="row">
                        <b>{{$greeting}} {{$timeclockuser->first_name}},</b> <i> what would you like to do?</i>
                    </div>
                    @if(is_null($entries))

                        <div class="form-group mt-2">
                            <label for="exampleFormControlSelect1"><b>Select Building</b></label>
                            <select class="form-control form-control-lg" name="bld" id="exampleFormControlSelect1" required>
                                <option value="">[Select Building]</option>
                                <option value="Outsource Bld 1">Outsource Bld 1</option>
                                <option value="Outsource Bld 5">Outsource Bld 5</option>
                                <option value="Outsource Bld 11">Outsource Bld 11</option>
                                <option value="Outsource Bld 16">Outsource Bld 16</option>
                                <option value="Outsource Bld 23">Outsource Bld 23</option>
                                <option value="Outsource Bld 101">Outsource Bld 101</option>
                                <option value="Day Driver">Day Driver</option>
                            </select>
                        </div>

                        <div class="form-row">
                            <input type="submit" class="btn btn-success btn-lg btn-block mt-2" value="Clock In" onclick="getLocation()">
                        </div>
                    @else
                        @if(is_null($entries->out_time))
                            <div class="form-row">
                                <input type="submit" class="btn btn-danger btn-lg btn-block mt-2" value="Clock Out" onclick="getLocation()">
                            </div>
                        @else

                            <div class="form-group mt-2">
                                <label for="exampleFormControlSelect1"><b>Select Building</b></label>
                                <select class="form-control form-control-lg" name="bld" id="exampleFormControlSelect1" required>
                                    <option value="">[Select Building]</option>
                                    <option value="Outsource Bld 1">Outsource Bld 1</option>
                                    <option value="Outsource Bld 5">Outsource Bld 5</option>
                                    <option value="Outsource Bld 11">Outsource Bld 11</option>
                                    <option value="Outsource Bld 16">Outsource Bld 16</option>
                                    <option value="Outsource Bld 23">Outsource Bld 23</option>
                                    <option value="Outsource Bld 101">Outsource Bld 101</option>
                                    <option value="Day Driver">Day Driver</option>
                                </select>
                            </div>

                            <div class="form-row">
                                <input type="submit" class="btn btn-success btn-lg btn-block mt-2" value="Clock In" onclick="getLocation()">
                            </div>
                        @endif

                    @endif

                    <div class="card mt-2">
                        <div class="card-title">
                            <b>Weekly Timecard</b>
                        </div>
                        <div class="card-body">
                            @if($weekly_card->isEmpty())
                                <div class="alert alert-info">No punches for this week</div>
                            @else
                                <table class="table table-sm table-bordered">
                                    <thead>
                                    <tr>
                                        <td><b>Date</b></td>
                                        <td><b>IN</b></td>
                                        <td><b>OUT</b></td>
                                        <td><b>LT</b></td>
                                        <td align="right"><b>Total</b></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($weekly_card as $card)
                                        <tr>
                                            <td>{{$card->in_time->format('m-d-y')}}</td>
                                            <td>{{$card->in_time->format('h:i A')}}</td>
                                            <td>@if(is_null($card->out_time)) -- @else  {{$card->out_time->format('h:i A')}} @endif</td>
                                            <td>{{$card->less_lunch_min}}</td>
                                            <td align="right">{{$card->total_time}}</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="4" align="right"><b>Weekly Total</b></td>
                                        <td colspan="1" align="right">{{$weekly_card->sum('total_time')}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>

                    <hr>
                    <div class="form-row">
                        <a href="#" class="btn btn-primary btn-lg btn-block mt-2 disabled">Manual Entry</a>
                    </div>
                    <div class="form-row">
                        <a href="/timeclock" class="btn btn-primary btn-lg btn-block mt-2 ">Cancel Punch</a>
                    </div>
                </form>
            </div>
        </div>
    </div>








@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">
        window.onload = showPosition;

        //Get GPS
        //var x = document.getElementById("long");

        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition, showError);
            } else {
                x.innerHTML = "Geolocation is not supported by this browser.";
            }
        }

        function showPosition(position) {
            //x.innerHTML = "Latitude: " + position.coords.latitude +
            //    "<br>Longitude: " + position.coords.longitude;
            document.getElementById("long").value = position.coords.longitude;
            document.getElementById("lat").value = position.coords.latitude;
        }

        function showError(error) {
            switch(error.code) {
                case error.PERMISSION_DENIED:
                    document.getElementById("long").value = "User denied the request for Geolocation."
                    break;
                case error.POSITION_UNAVAILABLE:
                    x.innerHTML = "Location information is unavailable."
                    break;
                case error.TIMEOUT:
                    x.innerHTML = "The request to get user location timed out."
                    break;
                case error.UNKNOWN_ERROR:
                    x.innerHTML = "An unknown error occurred."
                    break;
            }
        }

        window.onload = function() {
            getLocation();
        };
        //END GPS

    </script>

@endsection
