<!-- Modal -->
<div class="modal fade" id="newuser" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form  method="post" action="/timeclock/newuser">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add New Time Clock User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleFormControlInput1"><b>First Name</b></label>
                        <input type="text" class="form-control" name="first_name" id="exampleFormControlInput1" placeholder="First Name" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1"><b>Last Name</b></label>
                        <input type="text" class="form-control" name="last_name" id="exampleFormControlInput1" placeholder="Last Name" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1"><b>Time Clock ID</b></label>
                        <input type="text" class="form-control" name="timeclock_id" id="exampleFormControlInput1" placeholder="Time clock ID" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1"><b>Temp Company</b></label>
                        <input type="text" class="form-control" name="company" id="exampleFormControlInput1" placeholder="Temp Company">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Add User">
                </div>
            </form>
        </div>
    </div>
</div>
