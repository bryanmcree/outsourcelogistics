<!-- Modal -->
<div class="modal fade" id="punch_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form method="POST" action="/timeclock/update_punch">
                @csrf
                <input type="hidden" name="manual_approved_by" value="{{Auth::user()->id}}">
                <input type="hidden" class="id" name="id">
                <input type="hidden" name="punch_type" value="Manual">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><b>Punch Detail</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1"><b>In Time</b></label>
                                <input type="datetime-local" name="in_time" class="form-control in_time">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1"><b>Out Time</b></label>
                                <input type="datetime-local" name="out_time" class="form-control out_time" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="exampleFormControlSelect1"><b>Building Location</b></label>
                                <select class="form-control building_location" name="building_location" id="exampleFormControlSelect1" required>
                                    <option value="Outsource Bld 1">Outsource Bld 1</option>
                                    <option value="Outsource Bld 5">Outsource Bld 5</option>
                                    <option value="Outsource Bld 11">Outsource Bld 11</option>
                                    <option value="Outsource Bld 16">Outsource Bld 16</option>
                                    <option value="Outsource Bld 23">Outsource Bld 23</option>
                                    <option value="Outsource Bld 101">Outsource Bld 101</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label for="exampleInputEmail1"><b>Total Time</b></label>
                            <input type="text" name="out_time" class="form-control total_time" disabled>
                        </div>
                        <div class="col-sm-3">
                            <label for="exampleInputEmail1"><b>Total Min</b></label>
                            <input type="text" name="out_time" class="form-control total_min" disabled>
                        </div>
                        <div class="col-sm-3">
                            <label for="exampleInputEmail1"><b>Less Min</b></label>
                            <input type="number" name="less_lunch_min" class="form-control less_lunch_min">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1"><b>Notes</b></label>
                                <textarea class="form-control punch_notes" name="notes" id="exampleFormControlTextarea1" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                        <label class="form-check-label" for="exampleCheck1"><b>Accept this punch as is.</b></label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Edit Punch">
                </div>
            </form>
        </div>
    </div>
</div>
