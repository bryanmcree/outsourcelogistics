{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Timeclock Admin') == FALSE)
        @include('layouts.unauthorized')

    @Else
        <div class="card mb-4">
            <div class="card-header text-white" style="background-color: #413c69;">
                <b>Time Clock Management</b>
                <div class="btn-group btn-group-sm float-right">
                    <a href="#" class="btn btn-warning btn-sm disabled" title="Enter manual punch">Manual Punch</a>
                    <a href="/timeclock/users" class="btn btn-primary btn-sm" title="Manage employees who can clock in">Manage Users</a>
                    <a href="#" class="btn btn-secondary btn-sm disabled" title="Manage temp agencies">Manage Agencies</a>
                    <a href="#" class="btn btn-secondary btn-sm disabled" title="Manage exceptions such as vacation, call out and no shows">Manage Exceptions</a>
                </div>
            </div>
            <div class="card-body" id="timeclockDashboard">
                {{--Content loaded from Ajax--}}
            </div>
        </div>
    @endif
@include('timecard.modal.punchedit')
@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        //Auto Update
        $(document).ready(function() {
            $('#timeclockDashboard').load('/timeclock/dashboard/feed');
            var refreshId2 = setInterval(function() {
                $("#timeclockDashboard").load('/timeclock/dashboard/feed');
                //alert('reloaded')
            }, 1200000);  //1000 = one second
            $.ajaxSetup({ cache: false });
        });
        //END Auto Update

        //Punch Detail
        $(document).on("click", ".punchDetail", function () {
            //alert($(this).attr("data-id"));

            $('.id').val($(this).attr("data-id"));
            $('.in_time').val($(this).attr("data-in_time"));
            $('.out_time').val($(this).attr("data-out_time"));
            $('.last_name').val($(this).attr("data-last_name"));
            $('.building_location').val($(this).attr("data-building_location"));
            $('.punch_notes').val($(this).attr("data-notes"));

            $('.total_time').val($(this).attr("data-total_time"));
            $('.total_min').val($(this).attr("data-total_min"));

            $('.less_lunch_min').val($(this).attr("data-less_lunch_min"));


            $('#punch_detail').modal('show');
            //alert($(this).attr("data-first_name"));
        });
        //END Punch Detail

    </script>

@endsection
