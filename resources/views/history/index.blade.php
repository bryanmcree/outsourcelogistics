{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Admin') == FALSE)
        @include('layouts.unauthorized')

    @Else


        <div class="card mb-4">
            <div class="card-header" style="background-color: #91bcff;">
                <b>History</b> ({{$events->count()}})
            </div>
            <div class="card-body">
                <table class="table table-sm table-hover" id="history">
                    <thead>
                        <tr>
                            <td><b>Action</b></td>
                            <td><b>User</b></td>
                            <td><b>Desc</b></td>
                            <td><b>IP</b></td>
                            <td><b>Account</b></td>
                            <td><b>Created At</b></td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($events as $event)
                        <tr>
                            <td>{{$event->action}}</td>
                            <td>{{$event->user_id}}</td>
                            <td>{{$event->search_string}}</td>
                            <td>{{$event->user_ip}}</td>
                            <td>{{$event->account_type}}</td>
                            <td>{{$event->created_at}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#history2').DataTable();
        } );

    </script>

@endsection
