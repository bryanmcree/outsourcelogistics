{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.applicant')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')


        <div class="card mt-3">
            <div class="card-header" style="background-color: #91bcff;">
                <b>Create New Applicant Account</b>
            </div>
            <div class="card-body">
                <form method="post" action="/applicant/register/add" class="needs-validation" novalidate>
                    @csrf
                    <div class="form-row">
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom01"><b>First Name</b></label>
                            <input type="text" class="form-control" id="validationCustom01" name="first_name" placeholder="First name" required>
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom06"><b>Middle Name</b></label>
                            <input type="text" class="form-control" id="validationCustom06" name="middle_name" placeholder="Middle name">
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom02"><b>Last Name</b></label>
                            <input type="text" class="form-control" id="validationCustom02" name="last_name" placeholder="Last name" required>
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom16"><b>Alias / Nicknames</b></label>
                            <input type="text" class="form-control" id="validationCustom16" name="alias" placeholder="Alias / Nicknames">
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom07"><b>Address</b></label>
                            <input type="text" class="form-control" id="validationCustom07" name="address" placeholder="Address" required>
                            <div class="invalid-feedback">
                                Please provide an address.
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom08"><b>Apt / Suite</b></label>
                            <input type="text" class="form-control" id="validationCustom08" name="address2" placeholder="Apt / Suite">
                            <div class="invalid-feedback">
                                Please provide an address.
                            </div>
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationCustom03"><b>City</b></label>
                            <input type="text" class="form-control" id="validationCustom03" name="city" placeholder="City" required>
                            <div class="invalid-feedback">
                                Please provide a valid city.
                            </div>
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationCustom04"><b>State</b></label>
                            <select class="form-control" id="validationCustom04" name="state" required>
                                <option selected value="">[Select State]</option>
                                @foreach($states as $state)
                                    <option value="{{$state->code}}">{{$state->name}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Please provide a valid state.
                            </div>
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationCustom05"><b>Zip</b></label>
                            <input type="text" class="form-control" id="validationCustom05" name="zip" placeholder="Zip" required>
                            <div class="invalid-feedback">
                                Please provide a valid zip.
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom09"><b>Home Phone</b></label>
                            <input type="text" class="form-control" id="validationCustom09" name="home_phone" placeholder="Home Phone">
                            <div class="invalid-feedback">
                                Please provide a home phone number.
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom10"><b>Cell Phone</b></label>
                            <input type="text" class="form-control" id="validationCustom11" name="cell_number" placeholder="Cell Phone" required>
                            <div class="invalid-feedback">
                                Please provide a cell number.
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom12"><b>Date of Birth</b></label>
                            <input type="date" class="form-control" id="validationCustom12" name="dob" required>
                            <div class="invalid-feedback">
                                Please provide your date of birth.
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom13"><b>Social Security Number</b></label>
                            <input type="text" class="form-control" id="validationCustom13" name="ssn" placeholder="SSN" required>
                            <div class="invalid-feedback">
                                Please provide your social security number.
                            </div>
                        </div>
                    </div>
                    The below email and password will be used to access the Portal in the future.
                    <div class="form-row">
                        <div class="col-md-4 mb-3">
                            <label for="validationCustom09"><b>Email Address</b></label>
                            <input type="email" class="form-control" id="validationCustom09" name="email" placeholder="Email Address" required>
                            <div class="invalid-feedback">
                                Please provide a valid email address.
                            </div>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="validationCustom10"><b>Password</b></label>
                            <input type="password" class="form-control" id="validationCustom11" name="password" placeholder="Password" required>
                            <div class="invalid-feedback">
                                Please provide a password.
                            </div>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="validationCustom13"><b>Confirm Password</b></label>
                            <input type="password" class="form-control" id="validationCustom13" name="confirm_password" placeholder="Confirm Password" required>
                            <div class="invalid-feedback">
                                Please confirm your password.
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-primary" type="submit">Create Account</button>
                </form>
            </div>
        </div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function() {
            'use strict';
            window.addEventListener('load', function() {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();

    </script>

@endsection
