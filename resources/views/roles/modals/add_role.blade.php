<!-- Modal -->
<div class="modal fade" id="addRole" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="post" action="/roles/add">
                @csrf
                <div class="modal-header text-white" style="background-color: #1a32ba;">
                    <h5 class="modal-title" id="exampleModalLongTitle" >Add Role</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleFormControlInput1"><b>Role Name</b></label>
                        <input type="text" class="form-control" name="name" id="exampleFormControlInput1" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1"><b>Role Description</b></label>
                        <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="2"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                    <input type="submit" value="Add Role" class="btn-primary btn-sm btn">
                </div>
            </form>
        </div>
    </div>
</div>
