{{--New file Template--}}

{{--Add Security for this page below--}}
@if( Auth::user()->hasRole('Admin') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')


    <div class="card mb-4">
        <div class="card-header text-white" style="background-color: #413c69;">
            <b>OL Employee Warehouse Security Roles</b>
            <div class="float-right">
                <a href="#" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addRole"><i class="fad fa-plus-circle"></i> Add Role</a></div>
        </div>
        <div class="card-body">
            <table class="table table-hover table-sm" id="roles" width="100%">
                <thead>
                    <tr>
                        <td><b>Role Name</b></td>
                        <td><b>Role Description</b></td>
                        <td><b>Created At</b></td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                @foreach($allRoles as $role)
                    <tr>
                        <td>{{$role->name}}</td>
                        <td>{{$role->description}}</td>
                        <td>{{$role->created_at}}</td>
                        <td align="right">[Button]</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@include('roles.modals.add_role')
@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="text/javascript">
    $(document).ready( function () {
        $('#roles').DataTable();
    } );
</script>

@endsection

@endif
