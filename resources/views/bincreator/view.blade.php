{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Admin') == FALSE)
        @include('layouts.unauthorized')

    @Else


        <div class="card mb-4">
            <div class="card-header text-white" style="background-color: #413c69;">
                <b>Bins</b>
                <div class="btn-group float-right">
                    <a href="/bincreator" class="btn btn-sm btn-primary">New List</a>
                    <form action="/bincreator/3x2" method="POST" target="_blank" >
                        @csrf
                        <input type="hidden" name="delimiter" value="{{$delimiter}}">
                        <input type="hidden" name="building_number" value="{{$building_number}}">
                        <input type="hidden" name="aisle_number" value="{{$aisle_number}}">
                        <input type="hidden" name="start_number" value="{{$start_number}}">
                        <input type="hidden" name="end_number" value="{{$end_number}}">
                        <input type="submit" name="LabelSize" value="3x2" class="btn btn-sm btn-primary">
                        <input type="submit" name="LabelSize" value="4x6" class="btn btn-sm btn-primary">
                    </form>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-sm">
                    <thead>
                        <tr>
                            <td><b>Bin Number</b></td>
                            <td><b>Length</b></td>
                            <td><b>Pass</b></td>
                            <td><b>Human Read</b></td>
                            <td><b>Barcode Read</b></td>
                            <td><b>Code</b></td>
                        </tr>
                    </thead>
                    <tbody>
                    @for ($i = $start_number; $i <= $end_number; $i++)
                        <tr>
                            <td>{{$preBin . sprintf("%0". $end_number_length."d", $i)}}</td>
                            <td>{{strlen($preBin . sprintf("%0". $end_number_length."d", $i))}}</td>
                            <td>
                                @if(strlen($preBin . sprintf("%0". $end_number_length."d", $i)) <= 10)
                                    <i class="far fa-check" style="color: green;"></i>
                                @else
                                    <i class="fad fa-times-circle" style="color: red;"></i> FAIL - Too Long!
                                @endif
                            </td>
                            <td>{{$preBin . sprintf("%0". $end_number_length."d", $i)}}</td>
                            <td>{{$preBin . sprintf("%0". $end_number_length."d", $i)}}</td>
                            <td>{!! QrCode::size(50)->generate($preBin . sprintf("%0". $end_number_length."d", $i)); !!}
                            </td>
                        </tr>
                    @endfor
                    </tbody>
                </table>
            </div>
        </div>
    @endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection
