{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Admin') == FALSE)
        @include('layouts.unauthorized')

    @Else


        <div class="card mb-4">
            <div class="card-header text-white" style="background-color: #413c69;">
                <b>Moose EPIC fantastic state of the art BIN CREATOR!</b>
            </div>
            <div class="card-body">
                <form method="post" action="/bincreator/post">
                    @csrf
                    <ul class="list-group">
                        <li class="list-group-item">Total length for bin locations is 10 characters.  This includes dashes.</li>
                        <li class="list-group-item">A delimiter is required as this number will be printed on the pick sheets.</li>
                    </ul>
                    <div class="text-center" style="font-size: 25px; font-weight: bold;">
                        [Building #]-[Aisle]-[Row]
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Delimiter</b></label>
                        <input type="text" name="delimiter" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="-" required>
                        <small id="emailHelp" class="form-text text-muted">This is the character that will split the octets of the bin location.  Default is DASH.</small>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Building Number</b></label>
                        <input type="text" name="building_number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Building Number" required>
                        <small id="emailHelp" class="form-text text-muted">This is the number of the building.  This prevents duplicate bin locations in Camelot.</small>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Aisle Number</b></label>
                        <input type="text" name="aisle_number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Aisle Number" required>
                        <small id="emailHelp" class="form-text text-muted">This is the number of the aisle.  This could also be a letter like "D" for Dock or "S" for Stage.</small>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Starting Location Number</b></label>
                        <input type="text" name="start_number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Start Number" required>
                        <small id="emailHelp" class="form-text text-muted">This is the start number.  If you want location 1 - 5 you will put "1" in this field.</small>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Ending Location Number</b></label>
                        <input type="text" name="end_number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Ending Number" required>
                        <small id="emailHelp" class="form-text text-muted">This is the start number.  If you want location 1 - 5 you will put "5" in this field.</small>
                    </div>
                    <input type="submit" class="btn btn-sm btn-primary">
                </form>
            </div>
        </div>
    @endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection
