{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.print')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Admin') == FALSE)
        @include('layouts.unauthorized')

    @Else
        <style>
            .table {
                display: table;
                width: 400px;
                border-spacing: 1em .5em;
                padding: 0 0em 0em 0;
            }
            .tr {
                display: table-row;
                border-spacing: 0px;
                padding: 0px;
            }
            .td {
                display: table-cell;
                text-align: center;
                vertical-align: middle;
            }

            @media print {
                .pagebreak { page-break-after: always; } /* page-break-after works, as well */

            }
            @media print{
                @page {
                    size:auto;
                    margin-top: 0.15in;
                    margin-bottom: 0.01in;
                    margin-left: 0.01in;
                    margin-right: 0.01in;
                }

            }


            body
            {
                size: 3in 2in;
                margin: 0.0cm;
            }
        </style>


            @for($i = $start_location; $i <= $end_location; $i++)

                <div class="table">
                    <div class="tr">
                        <div class="td" style="vertical-align: middle">{!! QrCode::size(155)->generate($preBin . sprintf("%0". $end_location_length."d", $i) . $delimiter . $sub_location); !!}</div>
                    </div>
                    <div class="tr">
                        <div class="td" style="font-weight: bold; font-size: 65px; vertical-align: middle;">{{$preBin . sprintf("%0". $end_location_length."d", $i) . $delimiter . $sub_location}}</div>
                    </div>
                </div>
                <div class="pagebreak"> </div>
            @endfor

    @endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection
