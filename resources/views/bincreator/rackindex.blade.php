@extends('layouts.app')

@section('content')

    @if( Auth::user()->hasRole('Admin') == FALSE)
        @include('layouts.unauthorized')

    @Else


        <div class="card mb-4">
            <div class="card-header text-white" style="background-color: #413c69;">
                <b>Rack Bin Creator</b>
            </div>
            <div class="card-body">
                <form method="post" action="/bincreator/rackpost">
                    @csrf
                    <ul class="list-group">
                        <li class="list-group-item">Total length for bin locations is 10 characters.  This includes dashes.</li>
                        <li class="list-group-item">A delimiter is required as this number will be printed on the pick sheets.</li>
                    </ul>
                    <div class="text-center" style="font-size: 25px; font-weight: bold;">
                        R-[Row]-[Location]-[Sub Location]
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Delimiter</b></label>
                        <input type="text" name="delimiter" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="-" required>
                        <small id="emailHelp" class="form-text text-muted">This is the character that will split the octets of the bin location.  Default is DASH.</small>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Row</b></label>
                        <input type="text" name="row" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Row" required>
                        <small id="emailHelp" class="form-text text-muted">The large divison of locations, rows, the first number following the R.</small>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Starting Location Number</b></label>
                        <input type="text" name="start_location" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Start Location" required>
                        <small id="emailHelp" class="form-text text-muted">This is the start location. locations front to back within the row.  If you want location 1 - 5 you will put "1" in this field.</small>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Ending Location Number</b></label>
                        <input type="text" name="end_location" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Ending Location" required>
                        <small id="emailHelp" class="form-text text-muted">This is the end location. Locations front to back within the row. If you want location 1 - 5 you will put "5" in this field.</small>
                    </div>
                     <div class="form-group">
                        <label for="exampleInputEmail1"><b>Sub Location</b></label>
                        <input type="text" name="sub_location" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Sub Location" required>
                        <small id="emailHelp" class="form-text text-muted">This is the sublocation. Where the PLT is vertically and left or right, ex. A1 or B2</small>
                    </div>
                    <input type="submit" class="btn btn-sm btn-primary">
                </form>
            </div>
        </div>
    @endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection