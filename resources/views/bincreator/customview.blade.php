{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Admin') == FALSE)
        @include('layouts.unauthorized')

    @Else


        <div class="card mb-4">
            <div class="card-header text-white" style="background-color: #413c69;">
                <b>Bins</b>
                <div class="btn-group float-right">
                    <a href="/bincreator/custom" class="btn btn-sm btn-primary">New List</a>
                    <form action="/bincreator/custom4x6" method="POST" target="_blank" >
                        @csrf
                        <input type="hidden" name="custom" value="{{$custom}}">
                        <input type="submit" name="customLabelSize" value="4x6" class="btn btn-sm btn-primary">
                    </form>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-sm">
                    <thead>
                        <tr>
                            <td><b>Bin Number</b></td>
                            <td><b>Length</b></td>
                            <td><b>Pass</b></td>
                            <td><b>Human Read</b></td>
                            <td><b>Barcode Read</b></td>
                            <td><b>Code</b></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$custom}}</td>
                            <td>{{strlen($custom)}}</td>
                            <td>
                                @if(strlen($custom) <= 10)
                                    <i class="far fa-check" style="color: green;"></i>
                                @else
                                    <i class="fad fa-times-circle" style="color: red;"></i> FAIL - Too Long!
                                @endif
                            </td>
                            <td>{{$custom}}</td>
                            <td>{{$custom}}</td>
                            <td>{!! QrCode::size(50)->generate($custom); !!}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    @endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection