{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.print')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')



    <table class="table table-sm table-hover table-bordered">
        <thead>
            <tr>
                <td><b>Employee</b></td>
                <td><b>Department</b></td>
                <td><b>Expense</b></td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{$expense->employee->first_name}} {{$expense->employee->last_name}}</td>
                <td></td>
                <td>{{$expense->trip_title}}</td>
            </tr>
        </tbody>
    </table>


    <table class="table table-sm table-hover table-bordered">
        <thead>
        <tr>
            <td></td>
            <td><b>Category</b></td>
            <td><b>Vendor</b></td>
            <td align="right"><b>Amount</b></td>

        </tr>
        </thead>
        <tbody>
        <?php $row_num = 0 ?>
        @foreach($expense->items as $items)
            <?php $row_num = $row_num +1 ?>
            <tr>
                <td>{{$row_num}}.</td>
                <td>{{$items->category}}</td>
                <td>{{$items->vendor}}</td>
                <td align="right">${{$items->amount}}</td>
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td align="right"><b>Total Due:</b></td>
            <td align="right"><b>${{number_format($expense->items->sum('amount'),2)}}</b></td>

        </tr>
        </tbody>

    </table>

    <div style='page-break-after:always'></div>


    <table class="table table-sm table-hover table-bordered">
        <thead>
        <tr>
            <td></td>
            <td><b>Category</b></td>
            <td><b>Amount</b></td>
            <td></td>

        </tr>
        </thead>
        <tbody>
        <?php $row_num = 0 ?>
        @foreach($expense->items as $items)
            <?php $row_num = $row_num +1 ?>
            <tr>
                <td>{{$row_num}}.</td>
                <td>{{$items->category}}</td>
                <td>${{$items->amount}}</td>
                <td align="right">
                    @if(is_Null($items->new_filename))
                    No image
                    @else
                        <img src="{{asset('storage/receipts/'. $items->new_filename)}}" height="150">
                        <br>
                        {{$items->notes}}
                    @endif
                </td>
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td align="right"><b>Total Due:</b></td>
            <td><b>${{number_format($expense->items->sum('amount'),2)}}</b></td>
            <td></td>

        </tr>
        </tbody>

    </table>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection
