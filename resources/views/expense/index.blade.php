{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')




        <div class="card mb-4">
            <div class="card-header text-white" style="background-color: #413c69;">
                <b>Expense / Travel Reimbursement</b>
            </div>
            <div class="card-body">
                <div class="card">
                    <div class="card-body">
                        <div class="card">
                            <div class="card-header " style="background-color: #a7c5eb;"><b>Open Expense Request</b></div>
                            <div class="card-body">
                                <a href="#" class="btn btn-block btn-sm btn-primary float-right d-block d-sm-none" data-toggle="modal" data-target="#NewExpense">Add Expense</a>
                                @if($pendingRequest->isEmpty())
                                    <div class="alert alert-info">You don't have any pending expense request
                                        <a href="#" class="btn btn-sm btn-primary float-right" data-toggle="modal" data-target="#NewExpense">Add Expense</a>
                                    </div>
                                @else
                                    <table class="table table-sm table-hover" id="">
                                        <thead>
                                            <tr>
                                                <td><b>Title</b></td>
                                                <td align="right"><b>Amount</b></td>
                                                <td align="center" class="d-none d-sm-table-cell"><b>Status</b></td>
                                                <td class="d-none d-sm-table-cell"><b>Created At</b></td>
                                                <td class="d-none d-sm-table-cell"><b>Created By</b></td>
                                                <td align="right"><a href="#" class="btn btn-sm btn-primary float-right d-none d-sm-block" data-toggle="modal" data-target="#NewExpense">Add Expense</a></td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($pendingRequest as $pending)
                                            <tr>
                                                <td>{{$pending->trip_title}}</td>
                                                <td align="right">${{number_format($pending->items->sum('amount'),2)}}</td>
                                                <td align="center" class="d-none d-sm-table-cell">Pending</td>
                                                <td class="d-none d-sm-table-cell">{{$pending->created_at->format('m-d-Y')}}</td>
                                                <td class="d-none d-sm-table-cell">{{$pending->employee->first_name}} {{$pending->employee->last_name}}</td>
                                                <td align="right">

                                                    <a href="expense/{{$pending->id}}" class="btn btn-sm btn-secondary"><i class="fad fa-info-circle"></i>

                                                    </a> </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td></td>
                                            <td align="right">${{number_format($grand_total_pending['grand_total'], 2)}}</td>
                                            <td colspan="4"></td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                @endif
                            </div>
                        </div>
                        <div class="card mt-3">
                            <div class="card-header  " style="background-color: #a7c5eb;;"><b>Completed Expense Request</b></div>
                            <div class="card-body">
                                @if($completedRequest->isEmpty())
                                    <div class="alert alert-info">You don't have any complete expense request
                                    </div>
                                @else
                                    <table class="table table-sm table-hover" id="completed">
                                        <thead>
                                        <tr>
                                            <td><b>Title</b></td>
                                            <td align="right"><b>Amount</b></td>
                                            <td align="center" class="d-none d-sm-table-cell"><b>Status</b></td>
                                            <td class="d-none d-sm-table-cell"><b>Created At</b></td>
                                            <td class="d-none d-sm-table-cell"><b>Created By</b></td>
                                            <td align="right"><a href="#" class="btn btn-sm btn-primary float-right d-none d-sm-block" data-toggle="modal" data-target="#NewExpense">Add Expense</a></td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($completedRequest as $completed)
                                            <tr>
                                                <td>{{$completed->trip_title}}</td>
                                                <td align="right">${{number_format($completed->items->sum('amount'), 2)}}</td>
                                                <td align="center" class="d-none d-sm-table-cell">@if(is_null('complete')) Pending @else Complete @endif</td>
                                                <td class="d-none d-sm-table-cell">{{$completed->created_at->format('m-d-Y')}}</td>
                                                <td class="d-none d-sm-table-cell">{{$completed->employee->first_name}} {{$completed->employee->last_name}}</td>
                                                <td align="right">

                                                    <a href="expense/{{$completed->id}}" class="btn btn-sm btn-secondary"><i class="fad fa-info-circle"></i>

                                                    </a> </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td></td>
                                                <td align="right">${{number_format($grand_total['grand_total'], 2)}}</td>
                                                <td colspan="4"></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@include('expense.modal.new_expense')
@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#completed').DataTable();
        } );

    </script>

@endsection
