<nav class="navbar navbar-expand-md navbar-dark  mb-3" style="background-color: #709fb0;">

        <a class="navbar-brand" href="{{ url('/') }}">
            OSL
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
                @guest
                    <li class="nav-item active">
                        <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                    </li>
                @else
                    <li class="nav-item active">
                        <a class="nav-link" href="/home">Home <span class="sr-only">(current)</span></a>
                    </li>

                    @if ( Auth::user()->hasRole('Gate Control'))
                        <li class="nav-item">
                            <a class="nav-link" href="/gate">Gate</a>
                        </li>
                    @endif

                    @if ( Auth::user()->hasRole('Yard'))
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Yard
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="/yard" target="_blank"><i class="fad fa-parking"></i> Yard Dashboard</a>
                                <a class="dropdown-item" href="/yard/switch" target="_blank"><i class="fad fa-tablet-android-alt"></i> Switchers Dashboard</a>
                            </div>
                        </li>
                    @endif

                    @if ( Auth::user()->hasRole('Asset Control'))
                        <li class="nav-item">
                            <a class="nav-link" href="/asset">Asset Control</a>
                        </li>
                    @endif

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Dashboards
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="/dashboard/osl" target="_blank"><i class="fad fa-warehouse-alt"></i> Camelot</a>
                            @if ( Auth::user()->hasRole('Daily Charges'))
                            <a class="dropdown-item" href="/dailycharges" target="_blank"><i class="fad fa-hand-holding-usd"></i> Camelot Daily Charges</a>
                            @endif
                            @if ( Auth::user()->hasRole('Power Bill Dashboard'))
                                <a class="dropdown-item" href="/powerbill" target="_blank"><i class="fad fa-plug"></i> Warehouse Power Bill</a>
                            @endif
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            HR
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="/pto"><i class="fad fa-island-tropical"></i> PTO Request</a>
                            <a class="dropdown-item" href="/expense"><i class="fad fa-route"></i> Expense / Travel</a>
                            <a class="dropdown-item" href="#"><i class="fad fa-folder-open"></i> Forms</a>
                            <a class="dropdown-item" href="#"><i class="fad fa-user-friends"></i> Employee List</a>
                            <a class="dropdown-item" href="/timeclock/admin"><i class="fad fa-user-clock"></i> Time Card Management</a>
                            <a class="dropdown-item" href="#"><i class="fad fa-file-search"></i> Policies</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="https://momee.prismhr.com/mom/cmd/login" target="_blank"><i class="fad fa-money-check-edit"></i> Payroll</a>
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            IT
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="/barcode" ><i class="fad fa-barcode-alt"></i> Barcodes</a>
                            <a class="dropdown-item" href="/bins" ><i class="fad fa-container-storage"></i> Bin Transfer</a>
                            <a class="dropdown-item" href="/bincreator" ><i class="fad fa-container-storage"></i> Bin Creator</a>
                            <a class="dropdown-item" href="/bincreator/rack"><i class="fad fa-container-storage"></i> Rack Bin Creator </a>
                            <a class="dropdown-item" href="/bincreator/custom"><i class="fad fa-container-storage"></i> Custom Bin Creator </a>
                            <a class="dropdown-item" href="https://admin.google.com/" target="_blank"><i class="fab fa-google"></i> Google Admin</a>
                            <a class="dropdown-item" href="https://outsourcelogistics.on.spiceworks.com/portal/tickets" target="_blank"><i class="fad fa-ticket-alt"></i> Support Tickets</a>
                            <a class="dropdown-item" href="https://unifi.ui.com/dashboard" target="_blank"><i class="fad fa-chart-network"></i> UniFi Dashboard</a>
                            <a class="dropdown-item" href="https://hostedvip.hargray.com/login#?location" target="_blank"><i class="fad fa-chart-network"></i> Hargray Phone</a>
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Links
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="https://www.navispherecarrier.com/find-loads/single" target="_blank"><i class="fad fa-external-link-alt"></i> NaviSphere</a>
                            <a class="dropdown-item" href="https://vpn-hdc-onprem.kroger.com/dana-na/auth/url_default/welcome.cgi" target="_blank"><i class="fad fa-external-link-alt"></i> Kroger ITS</a>
                            <a class="dropdown-item" href="https://outlo.camelot3plcloud.com/Excalibur110" target="_blank"><i class="fad fa-external-link-alt"></i> Camelot</a>
                            <a class="dropdown-item" href="https://outlo.camelot3plcloud.com/mobilelink" target="_blank"><i class="fad fa-scanner"></i> MobileLink</a>
                            <a class="dropdown-item" href="https://outlo.camelot3plcloud.com/MobileLink/admin/" target="_blank"><i class="fad fa-scanner"></i> MobileLink Admin</a>
                            <a class="dropdown-item" href="https://mom.prismhr.com/mom/auth/#/login?lang=en" target="_blank"><i class="fad fa-money-check"></i> Momentum</a>
                        </div>
                    </li>
                        @if ( Auth::user()->hasRole('Development'))
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Development
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="https://colorhunt.co/palette/258271" target="_blank"><i class="fad fa-palette"></i> Color Palette</a>
                            </div>
                        </li>
                        @endif
                    <li class="nav-item">
                        <a class="nav-link" href="/ship">Shipping Labels</a>
                    </li>
                @endguest
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @auth
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="modal" data-target="#EmpSearch">Employee Search</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->first_name }} {{ Auth::user()->last_name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            @if ( Auth::user()->hasRole('Admin'))
                            <a class="dropdown-item" href="/users"><i class="fad fa-users"></i> Users</a>
                            <a class="dropdown-item" href="/roles"><i class="fad fa-lock-alt"></i> Roles</a>
                            <a class="dropdown-item" href="/test_email"><i class="fad fa-mailbox"></i> Test Email</a>
                            <a class="dropdown-item" href="/logs"><i class="fad fa-lock-alt"></i> Logs</a>
                            <div class="dropdown-divider"></div>
                            @endif

                            <a class="dropdown-item" href="/users/editaccount/{{ Auth::user()->id }}"><i class="fad fa-mailbox"></i> Edit Account</a>

                                <a class="dropdown-item" href="/password/reset"><i class="fad fa-key"></i> Change Password</a>

                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <i class="fad fa-sign-out-alt"></i> {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endauth
            </ul>
        </div>
</nav>

@auth
<!-- Modal -->
<div class="modal fade" id="EmpSearch" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header text-white"  style="background-color: #1a32ba;">
                <h5 class="modal-title" id="exampleModalLabel"><b>Employee Search</b></h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table" id="employees" width="100%">
                    <thead>
                    <tr>
                        <td><b>Name</b></td>
                        <td><b>Email</b></td>
                        <td><b>Work #</b></td>
                        <td><b>Cell #</b></td>
                        <td align="center"><b>Ext</b></td>
                        @if ( Auth::user()->hasRole('Admin'))
                            <td><b>Admin</b></td>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($emps as $emp)
                        <tr>
                            <td>{{$emp->first_name}} {{$emp->last_name}}</td>
                            <td align="center"><a href="mailto:{{$emp->email}}"><i class="fad fa-envelope-square"></i></a> </td>
                            <td>{{$emp->work_phone}}</td>
                            <td>{{$emp->cell_number}}</td>
                            <td align="center">{{$emp->work_ext}}</td>
                            @if ( Auth::user()->hasRole('Admin'))
                            <td align="right">
                                <div class="btn-group">
                                    <div class="btn-group d-none d-sm-block" role="group" aria-label="Users List">
                                        <a href="/users/id/{{$emp->id}}" class="btn btn-sm" title="Modify User Security" target="_blank "><i class="fad fa-id-badge"></i></a>
                                        <a href="/security/{{$emp->id}}" class="btn btn-sm" title="Modify User Security"><i class="fad fa-user-lock" style="color:green;"></i></a>
                                        <a href="/users/edit/{{$emp->id}}" class="btn btn-sm" title="Edit User"><i class="fad fa-user-edit"></i></a>
                                        <a href="/users/login_as/{{$emp->id}}" class="btn btn-sm" title="Login as User"><i class="fad fa-user-shield" style="color:black;"></i></a>
                                        <a href="/users/delete/{{$emp->id}}" class="btn btn-sm" title="Delete User"><i class="fad fa-user-times" style="color:red;"></i></a>
                                    </div>

                                    <div class="dropdown d-block d-sm-none">
                                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fad fa-bars"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a href="/users/id/{{$emp->id}}" class="dropdown-item" target="_blank" title="Modify User Security"><i class="fad fa-id-badge"></i></a>
                                            <a href="/security/{{$emp->id}}" class="dropdown-item" title="Modify User Security"><i class="fad fa-user-lock" style="color:green;"></i></a>
                                            <a href="/users/edit/{{$emp->id}}" class="dropdown-item" title="Edit User"><i class="fad fa-user-edit"></i></a>
                                            <a href="/users/login_as/{{$emp->id}}" class="dropdown-item" title="Login as User"><i class="fad fa-user-shield" style="color:black;"></i></a>
                                            <a href="/users/delete/{{$emp->id}}" class="dropdown-item" title="Delete User"><i class="fad fa-user-times" style="color:red;"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endauth

