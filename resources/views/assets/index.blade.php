{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Asset Control') == FALSE)
        @include('layouts.unauthorized')

    @Else


        <div class="card mb-4">
            <div class="card-header text-white" style="background-color: #413c69;">
                <b>Asset Control</b> ({{$assets->count()}})

                <div class="btn-group float-right" role="group" aria-label="Basic example">
                    <a href="asset/add" class="btn btn-sm btn-primary"><i class="fad fa-plus-circle"></i> Add Asset</a>
                </div>

            </div>
            <div class="card-body">
                <table class="table table-sm table-hover" id="assets">
                    <thead>
                        <tr>
                            <td><b>ID</b></td>
                            <td><b>Name</b></td>
                            <td><b>Department</b></td>
                            <td><b>Building</b></td>
                            <td><b>Unit Price</b></td>
                            <td><b>Qty</b></td>
                            <td><b>Assigned To</b></td>
                            <td><b>Model #</b></td>
                            <td><b>Serial #</b></td>
                            <td><b>GeoTab #</b></td>
                            <td><b>Condition</b></td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($assets as $asset)
                        <tr>
                            <td>{{sprintf("%05d", $asset->id)}}</td>
                            <td>{{$asset->name}}</td>
                            <td>{{$asset->department}}</td>
                            <td>{{$asset->building}}</td>
                            <td>${{number_format($asset->unit_price, 2)}}</td>
                            <td>{{$asset->unit_qty}}</td>
                            <td>{{$asset->assigned_to_name}}</td>
                            <td>{{$asset->model_number}}</td>
                            <td>{{$asset->serial_number}}</td>
                            <td>{{$asset->geo_tab_id}}</td>
                            <td>{{$asset->condition}}</td>
                            <td><a href="asset/{{$asset->id}}" class="btn btn-sm btn-primary float-right"><i class="fad fa-info-circle"></i> Detail</a> </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">
        $('div.dataTables_filter input').focus()
        $(document).ready( function () {
            $('#assets').DataTable({
                "pageLength": 50
            });
            $('div.dataTables_filter input').focus()
        } );
    </script>

@endsection
