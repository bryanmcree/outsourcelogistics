{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Asset Control') == FALSE)
        @include('layouts.unauthorized')

    @Else


        <div class="card mb-4">
            <div class="card-header text-white" style="background-color: #413c69;">
                <b>Asset Control - ADD ASSET</b>

                <div class="btn-group float-right" role="group" aria-label="Basic example">
                    <a href="/asset" class="btn btn-sm btn-primary"><i class="fad fa-caret-circle-left"></i> Back to Assets</a>
                </div>

            </div>
            <div class="card-body">
                <form method="post" action="/asset/new">
                    @csrf
                    <input type="hidden" name="created_by" value="{{Auth::user()->id}}">
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label for="inputName"><b>Name</b></label>
                            <input type="text" name="name" class="form-control" id="inputName" required>
                            <small id="emailHelp" class="form-text text-muted">Example:  Big Blue Widget</small>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputDescription"><b>Description</b></label>
                            <input type="text" name="description" class="form-control" id="inputDescription" required>
                            <small id="emailHelp" class="form-text text-muted">Example:  A big blue widget with a nylon case and instructions</small>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="inputDepartment"><b>Department</b></label>
                            <select id="inputDepartment" name="department" class="form-control" required>
                                <option selected>[Select]</option>
                                <option value="Warehouse">Warehouse</option>
                                <option value="Transportation">Transportation</option>
                                <option value="Billing">Billing</option>
                                <option value="Administration">Administration</option>
                                <option value="Human Resources">Human Resources</option>
                                <option value="Information Technology">Information Technology</option>
                            </select>
                        </div>


                        <div class="form-group col-md-2">
                            <label for="exampleFormControlSelect1"><b>Building</b></label>
                            <select class="form-control" name="building" id="exampleFormControlSelect1">
                                <option value="" selected>[Select One]</option>
                                @foreach($buildings as $building)
                                    <option value="{{$building->id}}">{{$building->building_name}}</option>
                                @endforeach
                            </select>
                        </div>



                        <div class="form-group col-md-2">
                            <label for="inputLocation"><b>Location</b></label>
                            <input type="text" name="location" class="form-control" id="inputLocation" >
                            <small id="emailHelp" class="form-text text-muted">Example:  In Robert's office in cabinet</small>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label for="inputPurchaseDate"><b>Purchase Date</b></label>
                            <input type="date" name="purchase_date" class="form-control" id="inputPurchaseDate">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="exampleFormControlSelect1"><b>Vendor</b></label>
                            <select class="form-control" name="building" id="exampleFormControlSelect1">
                                <option value="" selected>[Select One]</option>
                                @foreach($vendors as $vendor)
                                    <option value="{{$vendor->id}}">{{$vendor->vendor_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="inputPurchasePrice"><b>Purchase Price</b></label>
                            <input type="number" name="purchase_price" class="form-control" id="inputPurchasePrice" step="0.01" placeholder="0.00">
                            <small id="emailHelp" class="form-text text-muted">How much did we pay for this.</small>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="inputUnitPrice"><b>Unit Price</b></label>
                            <input type="number" name="unit_price" class="form-control" id="inputUnitPrice" placeholder="0.00" step="0.01">
                            <small id="emailHelp" class="form-text text-muted">How much did we pay for this.</small>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="inputUnitQty"><b>Unit Qty</b></label>
                            <input type="number" name="unit_qty" class="form-control" id="inputUnitQty" placeholder="0">
                            <small id="emailHelp" class="form-text text-muted">How many of these do we have.</small>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="inputCondition"><b>Condition</b></label>
                            <select id="inputCondition" name="condition" class="form-control">
                                <option selected>[Select]</option>
                                <option value="New">New</option>
                                <option value="Used">Used</option>
                                <option value="Refurbished">Refurbished</option>
                                <option value="Damaged">Damaged</option>
                            </select>
                            <small id="emailHelp" class="form-text text-muted">What was the condition when acquired.</small>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label for="inputModelNumber"><b>Model Number</b></label>
                            <input type="text" name="model_number" class="form-control" id="inputModelNumber">
                            <small id="emailHelp" class="form-text text-muted">Vendors model number for this asset.</small>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="inputSerialNumber"><b>Serial Number</b></label>
                            <input type="text" name="serial_number" class="form-control" id="inputSerialNumber">
                            <small id="emailHelp" class="form-text text-muted">Vendors serial number for this asset.</small>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="inputServiceTag"><b>Service Tag</b></label>
                            <input type="text" name="service_tag" class="form-control" id="inputServiceTag">
                            <small id="emailHelp" class="form-text text-muted">Service tag for asset.</small>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="inputVersion"><b>Version</b></label>
                            <input type="text" name="version" class="form-control" id="inputVersion">
                            <small id="emailHelp" class="form-text text-muted">Version number for asset.</small>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="inputWarrantyExpDate"><b>Warranty Exp Date</b></label>
                            <input type="date" name="warranty_exp_date" class="form-control" id="inputVersion">
                            <small id="emailHelp" class="form-text text-muted">Warranty expiration date.</small>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="inputCondition"><b>Category</b></label>
                            <select id="inputCondition" name="category" class="form-control">
                                <option selected>[Select]</option>
                                <option value="Computer">Computer</option>
                                <option value="Cellphone">Cellphone</option>
                                <option value="Laptop">Laptop</option>
                                <option value="CradlePoint">CradlePoint</option>
                                <option value="Tablet">Tablet</option>
                                <option value="GeoTab">GeoTab</option>
                                <option value="Network Switch">Network Switch</option>
                            </select>
                            <small id="emailHelp" class="form-text text-muted">Assign a category for this asset.</small>
                        </div>
                    </div>


                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label for="inputStaticIp"><b>Static IP</b></label>
                            <input type="text" name="static_ip" class="form-control" id="inputStaticIp">
                            <small id="emailHelp" class="form-text text-muted">If device has a static IP.</small>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="inputMACAddress"><b>MAC Address</b></label>
                            <input type="text" name="mac_address" class="form-control" id="inputMACAddress">
                            <small id="emailHelp" class="form-text text-muted">Devices MAC Address.</small>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="inputComputerName"><b>Computer Name</b></label>
                            <input type="text" name="computer_name" class="form-control" id="inputComputerName">
                            <small id="emailHelp" class="form-text text-muted">Network computer name.</small>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="inputGeoTabID"><b>GeoTab ID</b></label>
                            <input type="text" name="geo_tab_id" class="form-control" id="inputGeoTabID">
                            <small id="emailHelp" class="form-text text-muted">Communication number for GeoTab.</small>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="inputPhoneNumber"><b>Phone Number</b></label>
                            <input type="text" name="phone_number" class="form-control" id="inputPhoneNumber">
                            <small id="emailHelp" class="form-text text-muted">Devices phone number if assigned.</small>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="inputIMEI"><b>IMEI</b></label>
                            <input type="text" name="imei" class="form-control" id="inputIMEI">
                            <small id="emailHelp" class="form-text text-muted">Devices IMEI if assigned.</small>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="inputNotes"><b>Notes</b></label>
                            <textarea class="form-control" name="notes"></textarea>
                            <small id="emailHelp" class="form-text text-muted">Additional information regarding this asset.</small>
                        </div>
                    </div>

                    <input type="submit" class="btn btn-primary float-right" value="Add Asset">
                </form>
            </div>
        </div>
    @endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection
