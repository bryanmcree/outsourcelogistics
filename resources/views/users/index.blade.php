{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Admin') == FALSE)
        @include('layouts.unauthorized')

    @Else


    <div class="card mb-4">
        <div class="card-header text-white" style="background-color: #413c69;">
            <b>Users</b>
            <div class="btn-group float-right">
                <a href="#" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#AddUser">Add User</a>
                <a href="users/id/visitor/badge" target="_blank" class="btn btn-sm btn-primary">Visitor Badge</a>
                <a href="users/id/contractor/badge" target="_blank" class="btn btn-sm btn-primary">Contractor Badge</a>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-sm table-hover" id="users" width="100%">
                <thead>
                <tr>
                    <td></td>
                    <td><b>Name</b></td>
                    <td class="d-none d-sm-table-cell"><b>Title</b></td>
                    <td class="d-none d-sm-table-cell"><b>Status</b></td>
                    <td class="d-none d-sm-table-cell"><b>Email</b></td>
                    <td class="d-none d-sm-table-cell"><b>Work Phone</b></td>
                    <td class="d-none d-sm-table-cell"><b>Ext</b></td>
                    <td class="d-none d-sm-table-cell"><b>Cell Phone</b></td>
                    <td class="d-none d-sm-table-cell"><b>Created At</b></td>
                    <td></td>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>
                        @if(is_null($user->new_filename))
                            <i class="fad fa-user fa-4x AddImage" data-location_id = "{{$user->id}}"></i></td>
                        @else
                            <img class="AddImage" data-location_id = "{{$user->id}}" src="{{asset('storage/users/'. $user->new_filename)}}" height="75">
                        @endif
                        <td style="vertical-align:middle;">{{$user->first_name}} {{$user->last_name}}</td>
                        <td style="vertical-align:middle;" class="d-none d-sm-table-cell">{{$user->title}}</td>
                        <td style="vertical-align:middle;" class="d-none d-sm-table-cell">{{$user->employee_status}}</td>
                        <td style="vertical-align:middle;" class="d-none d-sm-table-cell">{{$user->email}}</td>
                        <td style="vertical-align:middle;" class="d-none d-sm-table-cell">{{$user->work_phone}}</td>
                        <td style="vertical-align:middle;" class="d-none d-sm-table-cell">{{$user->work_ext}}</td>
                        <td style="vertical-align:middle;" class="d-none d-sm-table-cell">{{$user->cell_number}}</td>
                        <td style="vertical-align:middle;" class="d-none d-sm-table-cell">{{$user->created_at->format('m-d-Y')}}</td>
                        <td style="vertical-align:middle;" align="center" nowrap="">
                            <div class="btn-group d-none d-sm-block" role="group" aria-label="Users List">
                                <a href="/users/id/{{$user->id}}" class="btn btn-sm" title="Modify User Security" target="_blank "><i class="fad fa-id-badge"></i></a>
                                <a href="/security/{{$user->id}}" class="btn btn-sm" title="Modify User Security"><i class="fad fa-user-lock" style="color:green;"></i></a>
                                <a href="/users/edit/{{$user->id}}" class="btn btn-sm" title="Edit User"><i class="fad fa-user-edit"></i></a>
                                <a href="/users/login_as/{{$user->id}}" class="btn btn-sm" title="Login as User"><i class="fad fa-user-shield" style="color:black;"></i></a>
                                <a href="/users/delete/{{$user->id}}" class="btn btn-sm" title="Delete User"><i class="fad fa-user-times" style="color:red;"></i></a>
                            </div>

                            <div class="dropdown d-block d-sm-none">
                                <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fad fa-bars"></i>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a href="/users/id/{{$user->id}}" class="dropdown-item" target="_blank" title="Modify User Security"><i class="fad fa-id-badge"></i></a>
                                    <a href="/security/{{$user->id}}" class="dropdown-item" title="Modify User Security"><i class="fad fa-user-lock" style="color:green;"></i></a>
                                    <a href="/users/edit/{{$user->id}}" class="dropdown-item" title="Edit User"><i class="fad fa-user-edit"></i></a>
                                    <a href="/users/login_as/{{$user->id}}" class="dropdown-item" title="Login as User"><i class="fad fa-user-shield" style="color:black;"></i></a>
                                    <a href="/users/delete/{{$user->id}}" class="dropdown-item" title="Delete User"><i class="fad fa-user-times" style="color:red;"></i></a>
                                </div>
                            </div>


                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @endif
@include('users.modal.upload_image')
    @include('users.modal.addUser')
@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#users').DataTable();
        } );


        $(document).on("click", ".AddImage", function () {
            $('.id').val($(this).attr("data-location_id"));
            $('#UploadImage').modal('show');
        });

        //Modal Valdiation

        // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function() {
            'use strict';
            window.addEventListener('load', function() {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>

@endsection
