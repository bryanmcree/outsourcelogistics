<!-- Modal -->
<div class="modal fade" id="UploadImage" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form method="POST" action="/user/upload/image" enctype="multipart/form-data">
                <input type="hidden" class="id" name="id">
                @csrf
                <div class="modal-header  bg-warning">
                    <h5 class="modal-title" id="exampleModalLabel">Upload Image</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleFormControlFile1"><b>Select Image</b></label>
                        <input type="file" name="file_name" class="form-control-file" id="exampleFormControlFile1">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Upload">
                </div>
            </form>
        </div>
    </div>
</div>
