<!-- Modal Template -->
<div class="modal fade" id="EditUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <form method="post" action="">
                @csrf
                <input type="hidden" name="client_id" value="{{$user->id}}">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col-md-4 mb-3">
                            <label for="validationServer01"><b>First Name</b></label>
                            <input type="text" class="form-control is-valid" id="validationServer01" placeholder="First name" value="{{$user->first_name}}" required>
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="validationServer06"><b>Middle Name</b></label>
                            <input type="text" class="form-control is-valid" id="validationServer06" placeholder="Middle Name" value="{{$user->middle_name}}" required>
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="validationServer02"><b>Last Name</b></label>
                            <input type="text" class="form-control is-valid" id="validationServer02" placeholder="Last name" value="{{$user->last_name}}" required>
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-5 mb-3">
                            <label for="validationServer07"><b>Address</b></label>
                            <input type="text" class="form-control is-invalid" id="validationServer07" placeholder="Address" value="{{$user->address}}" required>
                            <div class="invalid-feedback">
                                Please provide a valid address.
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="validationServer03">City</label>
                            <input type="text" class="form-control is-invalid" id="validationServer03" placeholder="City" required>
                            <div class="invalid-feedback">
                                Please provide a valid city.
                            </div>
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationServer04">State</label>
                            <input type="text" class="form-control is-invalid" id="validationServer04" placeholder="State" required>
                            <div class="invalid-feedback">
                                Please provide a valid state.
                            </div>
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationServer05">Zip</label>
                            <input type="text" class="form-control is-invalid" id="validationServer05" placeholder="Zip" required>
                            <div class="invalid-feedback">
                                Please provide a valid zip.
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                            <input class="form-check-input is-invalid" type="checkbox" value="" id="invalidCheck3" required>
                            <label class="form-check-label" for="invalidCheck3">
                                Agree to terms and conditions
                            </label>
                            <div class="invalid-feedback">
                                You must agree before submitting.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                    <input  type="submit" class="btn btn-sm btn-primary" value="Submit Form">
                </div>
            </form>
        </div>
    </div>
</div>
