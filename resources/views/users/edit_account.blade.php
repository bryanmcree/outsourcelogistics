{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if(Auth::user()->id <> $user->id)
        @include('layouts.unauthorized')

    @Else


        <div class="card mb-4">
            <div class="card-header" style="background-color: #91bcff;">
                <b>Edit User</b>
            </div>
            <div class="card-body">
                <form method="post" action="/user/editaccount/update" class="needs-validation" novalidate>
                    <input type="hidden" value="{{$user->id}}" name="id">
                    @csrf
                    <div class="form-row">
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom01"><b>First Name</b></label>
                            <input type="text" class="form-control" id="validationCustom01" name="first_name" placeholder="First Name" value="{{$user->first_name}}" required>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom01"><b>Middle Name</b></label>
                            <input type="text" class="form-control" id="validationCustom01" name="middle_name" placeholder="Middle Name" value="{{$user->middle_name}}">
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom02"><b>Last Name</b></label>
                            <input type="text" class="form-control" id="validationCustom02" name="last_name" placeholder="Last Name" value="{{$user->last_name}}" required>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom09"><b>Alias</b></label>
                            <input type="text" class="form-control" id="validationCustom09" name="alias" placeholder="Alias" value="{{$user->alias}}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-4 mb-3">
                            <label for="validationCustom07"><b>Address</b></label>
                            <input type="text" class="form-control" id="validationCustom07" name="address" placeholder="Address"  value="{{$user->address}}">
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom08"><b>Suite / Apt</b></label>
                            <input type="text" class="form-control" id="validationCustom08" name="address2" placeholder="Suite / Apt"  value="{{$user->address2}}">
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom03"><b>City</b></label>
                            <input type="text" class="form-control" id="validationCustom03" name="city" placeholder="City"  value="{{$user->city}}">
                            <div class="invalid-feedback">
                                Please provide a valid city.
                            </div>
                        </div>
                        <div class="col-md-1 mb-3">
                            <label for="validationCustom04"><b>State</b></label>
                            <select class="form-control" id="validationCustom04" name="state" >
                                <option selected  value="{{$user->state}}">{{$user->state}}</option>
                                @foreach($states as $state)
                                    <option value="{{$state->code}}">{{$state->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-1 mb-3">
                            <label for="validationCustom05"><b>Zip</b></label>
                            <input type="text" class="form-control" id="validationCustom05" name="zip" placeholder="Zip"  value="{{$user->zip}}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-2 mb-3">
                            <label for="validationCustom09"><b>Home Location</b></label>
                            <select class="form-control" id="validationCustom09" name="site_location" >
                                <option selected  value="{{$user->site_location}}">{{$user->site_location}}</option>
                                <option value="Savannah">Savannah</option>
                                <option value="Tifton">Tifton</option>
                                <option value="Valdosta">Valdosta</option>
                            </select>
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationCustom05"><b>Home Phone</b></label><div></div>
                            <input type="text" class="form-control" id="validationCustom05" name="home_phone" placeholder="Home Phone"  value="{{$user->home_phone}}" maxlength="10">
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationCustom05"><b>Work Phone</b></label>
                            <input type="text" class="form-control" id="validationCustom05" name="work_phone" placeholder="Work Phone"  value="{{$user->work_phone}}" maxlength="10">
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationCustom05"><b>Work Ext</b></label>
                            <input type="text" class="form-control" id="validationCustom05" name="work_ext" placeholder="Work Ext"  value="{{$user->work_ext}}">
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationCustom05"><b>Cell Number</b></label>
                            <input type="text" class="form-control" id="validationCustom05" name="cell_number" placeholder="Cell Number"  value="{{$user->cell_number}}" maxlength="10">
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationCustom05"><b>Fax Number</b></label>
                            <input type="text" class="form-control" id="validationCustom05" name="fax_number" placeholder="Fax Number"  value="{{$user->fax_number}}" maxlength="10">
                        </div>
                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="col-md-2 mb-3">
                            <label for="validationCustom01"><b>Date of Birth</b></label>
                            <input type="date" class="form-control" id="validationCustom01" name="dob"
                                   @if(is_null($user->dob))
                                   Value = ""
                                   @else
                                   value="{{$user->dob->format('Y-m-d')}}"
                                @endif
                            >
                        </div>
                        <div class="col-md-1 mb-3">
                            <label for="validationCustom01"><b>SSN</b></label>
                            <input type="text" class="form-control" id="validationCustom01" name="ssn" placeholder="SSN" maxlength="9"

                                   @if(is_null($user->ssn))
                                   value=""
                                   @else
                                   value="{{\Illuminate\Support\Facades\Crypt::decryptString($user->ssn)}}"
                                @endif

                            >
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom02"><b>Email</b></label>
                            <input type="text" class="form-control" id="validationCustom02" name="email" placeholder="Email" value="{{$user->email}}" disabled>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom09"><b>Title</b></label>
                            <input type="text" class="form-control" id="validationCustom09" name="title" placeholder="Title" value="{{$user->title}}">
                        </div>
                    </div>
                    <button class="btn btn-primary" type="submit">Update Account</button>
                </form>
            </div>
        </div>
    @endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );


        (function() {
            'use strict';
            window.addEventListener('load', function() {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();

    </script>

@endsection
