{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.print')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Admin') == FALSE)
        @include('layouts.unauthorized')

    @Else
        <br>
        <table width="100%"  border="0" cellspacing="0" cellpadding="3">
            <tr>
                <td align="center">
                    <img src="{{url('img/Outsource Logistics_500.png')}}" class="">
                </td>
            </tr>
            <tr>
                <td><br><br><br></td>
            </tr>
            <tr>
                <td WIDTH="50%" align="center" style="font-size: 50px;"><i STYLE="color: brown" class="fad fa-user fa-10x"></i></td>
            </tr>
            <tr>
                <td align="center" colspan="2" style="font-size: 90px;"></td>
            </tr>
            <tr>
                <td><br><br><br><br><br><br><br></td>
            </tr>
            <tr>
                <td align="center" bgcolor="red" colspan="2"  style="font-size: 100px; color: white;"><b>VISITOR</b></td>
            </tr>
        </table>

    @endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection
