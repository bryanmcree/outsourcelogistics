{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Admin') == FALSE)
        @include('layouts.unauthorized')

    @Else
        <div class="col-md-12">
            <div class="card mt-2">
                <div class="card-header text-white" style="background-color: #413c69;"><b>Update Security - {{$user->last_name}}, {{$user->first_name}}</b>
                    <a href="/users" class="btn btn-sm btn-primary float-right">Return to Users</a>
                </div>
                <div class="card-body">
                    <div class="card mt-2">
                        <div class="card-header" style="background-color: #a7c5eb;"><b>Assigned Roles</b></div>
                        <div class="card-body">
                            @if($assigned_roles->isEmpty())
                                <div class="alert alert-warning">No roles assigned</div>
                            @else
                                <form method="POST" action="{{ route('remove_roles') }}">
                                    @csrf
                                    <input type="hidden" name="user_id" value="{{$user->id}}">
                                    @foreach($assigned_roles as $role)
                                        <div class="form-check">
                                            <input class="form-check-input" name="remove_roles[]" type="checkbox" value="{{$role->id}}" id="{{$role->id}}">
                                            <label class="form-check-label" for="{{$role->id}}">
                                                <b>{{$role->name}}</b> - <i>{{$role->description}}</i>
                                            </label>
                                        </div>
                                    @endforeach
                                    <input type="submit" class="btn btn-danger mt-2" value="REMOVE Above Roles">
                                </form>

                            @endif
                        </div>
                    </div>
                    <div class="card mt-2">
                        <div class="card-header" style="background-color: #a7c5eb;"><b>Available Roles</b></div>
                        <div class="card-body">
                            @if($all_roles->isEmpty())
                                <div class="alert alert-warning">No roles assigned</div>
                            @else
                                <form method="POST" action="{{ route('add_roles') }}">
                                    @csrf
                                    <input type="hidden" name="user_id" value="{{$user->id}}">
                                    @foreach($all_roles as $role)
                                        <div class="form-check">
                                            <input class="form-check-input" name="add_roles[]" type="checkbox" value="{{$role->id}}" id="{{$role->id}}">
                                            <label class="form-check-label" for="{{$role->id}}">
                                                <b>{{$role->name}}</b> - <i>{{$role->description}}</i>
                                            </label>
                                        </div>
                                    @endforeach
                                    <input type="submit" class="btn btn-primary mt-2" value="Add Above Roles">
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection
