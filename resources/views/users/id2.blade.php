{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.print')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Admin') == FALSE)
        @include('layouts.unauthorized')

    @Else

        <table width="100%"  border="0" cellspacing="0" cellpadding="3">
            <tr>
                <td align="center" colspan="2">
                    <img src="{{url('img/Blue_Outsource Logistics_Transparent_500.png')}}" class="">
                </td>
            </tr>
            <tr>
                <td WIDTH="50%" align="center">
                    <img src="{{asset('storage/users/'. $user->new_filename)}}" height="600" >
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" style="font-size: 90px;"><b>{{$user->first_name}} {{$user->last_name}}</b></td>
            </tr>
            <tr>
                <td align="center" colspan="2"  style="font-size: 80px;"><b>{{$user->title}}</b></td>
            </tr>
        </table>
        <div style="position: fixed; right: 0mm; bottom: 0mm;">
            {!! QrCode::eyeColor(1, 50, 62, 167, 0, 0, 0)->eyeColor(2, 171, 12, 15, 0, 0, 0)->size(250)->generate('generic'. CHR(9) .'generic'); !!}
        </div>

        <div style="position: fixed; right: 0mm; bottom: 225mm; transform: rotate(90deg)">
            <img src="data:image/png;base64,{{DNS2D::getBarcodePNG($user->first_name . CHR(9) . $user->last_name . CHR(9) . $user->title, 'PDF417')}}" alt="barcode" />
        </div>
    @endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection
