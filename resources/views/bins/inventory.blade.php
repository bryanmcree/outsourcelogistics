{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Bin Transfers') == FALSE)
        @include('layouts.unauthorized')

    @Else


        <div class="card mb-4">
            <div class="card-header bg-success text-white">
                <b>Scan Bin Location INVENTORY</b>
                <a href="/bins" class="btn btn-sm btn-warning float-right"><i class="fad fa-home"></i></a>
            </div>
            <form method="post" action="/bins/inventory/bin">
                @csrf
                <div class="card-body">
                    <input class="form-control form-control-lg " name="bin_location" type="text" placeholder="Scan Bin Location" autofocus required>
                </div>
            </form>
        </div>
    @endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection
