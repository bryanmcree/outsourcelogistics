{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Bin Transfers') == FALSE)
        @include('layouts.unauthorized')

    @Else


        <div class="card mb-4">
            <div class="card-header" style="background-color: #91bcff;">
                <b>Bin Transfers</b>
            </div>
            <div class="card-body">
                <div class="card-deck">
                    <div class="card border-0 mb-5">
                        <a href="/bins/scan" class="btn btn-block btn-primary btn-lg">Scan</a>
                    </div>
                    <div class="card border-0">
                        <a href="/bins/dashboard" class="btn btn-block btn-primary btn-lg">Dashboard</a>
                    </div>
                    <div class="card border-0">
                        <a href="/bins/inventory" class="btn btn-block btn-success btn-lg">Inventory</a>
                    </div>
                    <div class="row mt-3">
                        <div class="col-8">
                            <b>Total Completed:</b>
                        </div>
                        <div class="col-4 text-right">
                            {{$allBins->where('bin_status','Complete')->count()}}
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-8">
                            <b>Total Pending:</b>
                        </div>
                        <div class="col-4 text-right">
                            {{$allBins->where('bin_status','Pending')->count()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection
