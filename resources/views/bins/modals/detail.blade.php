<!-- Modal Template -->
<div class="modal fade" id="BinDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <h5 class="modal-title" id="exampleModalLongTitle"><b>Bin Detail</b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="list-group">
                    <li class="list-group-item"><b>Bin Location: </b><span class="bin_location"></span></li>
                    <li class="list-group-item"><b>License Plate: </b> <span class="license_plate"></span></li>
                    <li class="list-group-item"><b>Status: </b> <span class="bin_status"></span></li>
                    <li class="list-group-item"><b>Created by: </b> <span class="created_by"></span></li>
                    <li class="list-group-item"><b>Created at: </b> <span class="created_at"></span></li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
