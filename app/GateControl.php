<?php

namespace App;

use App\Traits\Observable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GateControl extends Model
{
    use SoftDeletes;
    use Observable;

    public static function logSubject(Model $model): string
    {
        return sprintf( "User [id:%d] %s/%s",
            $model->id, $model->name, $model->email
        );
    }

    protected $guarded = [];
}
