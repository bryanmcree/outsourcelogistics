<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContainerNotes extends Model
{
    protected $table = 'container_notes';
    protected $guarded = [];

    public function createdBY(){
        return $this->hasOne('\App\User','id','created_by');
    }
}
