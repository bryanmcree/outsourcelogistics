<?php

namespace App\Exports;

use App\Shipments;
use Maatwebsite\Excel\Concerns\FromCollection;

class ShippingExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Shipments::selectRaw(
            'record_type,
            depositor_code,
            order_ref_number,
            purchase_order_number,
            shipment_id_number,
            DATE_FORMAT(order_date, "%m/%d/%Y") as order_date,
            DATE_FORMAT(order_date, "%m/%d/%Y") as ship_date,
            ship_to_code,
            ship_to_name,
            ship_to_name_2,
            ship_to_address,
            ship_to_address_2,
            ship_to_city,
            ship_to_state,
            ship_to_zip,
            ship_to_country,
            scac,
            carrier_name,
            freight_bill_type,
            pro_number,
            hdr_user_1,
            hdr_user_2,
            hdr_user_3,
            item_number,
            qty_ordered,
            qty_shipped,
            uom,
            desc_1,
            desc_2,
            lot_number,
            line_reference,
            line_user_1,
            line_user_2,
            grs_weight,
            net_weight,
            warehouse'
        )->get();
    }
}
