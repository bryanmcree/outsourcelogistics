<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PTO extends Model
{
    protected $guarded = [];
    protected $dates = ['start_date','end_date'];

    public function employee(){
        return $this->hasOne('\App\User','id','employee_id');
    }
}
