<?php

namespace App\Http\Controllers;

use App\PowerBill;
use App\User;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class PowerBillController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $useage = PowerBill::select('building','month','bill_cost','bill_date', 'created_at','id')
            ->orderby('bill_date')
            ->get();

        $lables = PowerBill::select('bill_date')
            ->groupby('bill_date')
            ->orderby('bill_date')
            ->get();

        $loc = PowerBill::select('building')
            ->groupby('building')
            ->orderby('building')
            ->get();
        //dd($loc);

        return view('powerbill.index', compact('useage','loc','lables'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PowerBill  $powerBill
     * @return \Illuminate\Http\Response
     */
    public function show(PowerBill $powerBill)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PowerBill  $powerBill
     * @return \Illuminate\Http\Response
     */
    public function edit(PowerBill $powerBill)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PowerBill  $powerBill
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PowerBill $powerBill)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PowerBill  $powerBill
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $user = PowerBill::where('id', $request->id)->delete();
        Alert::toast('Bill Deleted', 'success');
        return redirect('/powerbill');
    }

    public function add(Request $request)
    {
        $request->offsetSet('bill_date', $request->year . '-'. $request->month .'-01');
        $newData = $request->all();

        $newData2 = PowerBill::create($newData);
        //dd($request);

        Alert::toast('Bill Added!', 'success');
        return redirect('/powerbill');


    }
}
