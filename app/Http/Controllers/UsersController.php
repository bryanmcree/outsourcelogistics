<?php

namespace App\Http\Controllers;

use App\ExpenseItems;
use App\Role_User;
use App\Roles;
use App\States;
use App\User;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;
use Webpatser\Uuid\Uuid;
use Image;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
        $users = User::all();

        return view('users.index', compact('users'));
    }

    public function create()
    {
        return view('users.add_user', compact('states'));
    }

    public function store(Request $request)
    {
        //
        $request->offsetSet('password', Hash::make($request->password)); //encrypt password
        $user = $request->all();
        User::create($user);
        return redirect('/users');
    }

    public function edit($id)
    {
        $states = States::all();
        $user = User::findorfail($id);
        return view('users.edit', compact('user','states'));
    }

    public function editaccount($id)
    {
        $states = States::all();
        $user = User::findorfail($id);
        return view('users.edit_account', compact('user','states'));
    }

    public function update(Request $request)
    {
        $user = User::findorfail($request->id);
        $request->offsetSet('ssn', Crypt::encryptString($request->ssn));  //Encrypt SSN
        $user->update($request->all());
        Alert::toast('User has been updated!', 'success');
        return redirect('/users');
    }

    public function updateaccount(Request $request)
    {
        $user = User::findorfail($request->id);
        $request->offsetSet('ssn', Crypt::encryptString($request->ssn));  //Encrypt SSN
        $user->update($request->all());
        Alert::toast('Account has been updated!', 'success');
        return redirect('/home');
    }

    public function destroy(Request $request)
    {
        $user = User::where('id', $request->id)->delete();
        Alert::toast('User Deleted', 'success');
        return redirect('/users');
    }

    public function user_security($id){
        $user = User::find($id);
        $current_roles = Role_User::select('roles_id')->where('user_id','=', $id)
            ->get();
        //dd($current_roles);
        $all_roles = Roles::wherenotin('id', $current_roles)
            ->orderby('name')
            ->get();
        $assigned_roles = Roles::wherein('id', $current_roles)
            ->orderby('name')
            ->get();

        return view('users.security', compact('user','all_roles','current_roles','assigned_roles'));
    }

    public function remove_roles(Request $request){

        $user_id = $request->user_id;

        foreach($request->remove_roles as $role)
        {
            $delete = Role_User::where('user_id', '=', $request->user_id)
                ->where('roles_id', '=', $role)
                ->delete();
        }

        Alert::toast('Roles have been deleted!', 'success');

        return redirect('/security/' . $user_id);
    }

    public function add_roles(Request $request){

        $user_id = $request->user_id;

        foreach($request->add_roles as $role)
        {
            $newRoles = new Role_User();
            $newRoles->roles_id = $role;
            $newRoles->user_id = $request->user_id;
            $newRoles->save();
        }

        Alert::toast('Roles Updated', 'success');
        return redirect('/security/' . $user_id);
    }

    public function login_as($id){

        $user = User::where('id','=', $id)->first();
        //dd($user);
        Auth::login($user);
        return redirect('/home');

    }

    public function id($id)
    {
        $user = User::findorfail($id);

        return view('users.id', compact('user'));
    }

    public function id2($id)
    {
        $user = User::findorfail($id);

        return view('users.id2', compact('user'));
    }

    public function visitor_id()
    {
        return view('users.visitor');
    }

    public function contractor_id()
    {
        return view('users.contractor');
    }

    public function add_image(Request $request)
    {
        $user = User::where('id','=', $request->id)->first();


            $request->offsetSet('extension', $request->file_name->getClientOriginalExtension());
            $request->offsetSet('old_filename', $request->file_name->getClientOriginalName());
            $request->offsetSet('new_filename', Uuid::generate(4) .'.'.$request->extension);

            $image = $request->file('file_name');
            $input['imagename'] = $request->new_filename;

            $destinationPath = storage_path('app/thumbnail');
            $img = Image::make($image->path());
            $img->resize(1000, 1000, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$input['imagename']);

            $destinationPath = storage_path('app/users');
            $image->move($destinationPath, 'app/users/'.$input['imagename']);

            $file = new Filesystem();
            $file->cleanDirectory(storage_path('app/thumbnail'));



        $user->extension = $request->extension;
        $user->old_filename = $request->old_filename;
        $user->new_filename = $request->new_filename;
        $user->file_name = $request->file_name;
        $user->save();

        Alert::toast('Image Added!', 'success');
        return redirect('/users');
    }

    public function ManualAdd(Request $request)
    {
        //
        $request->offsetSet('password', Hash::make($request->password)); //encrypt password

        if(!is_null($request->file_name)){ //Prevents error if no receipt is attached
            $request->offsetSet('extension', $request->file_name->getClientOriginalExtension());
            $request->offsetSet('old_filename', $request->file_name->getClientOriginalName());
            $request->offsetSet('new_filename', Uuid::generate(4) .'.'.$request->extension);

            $image = $request->file('file_name');
            $input['imagename'] = $request->new_filename;

            $destinationPath = storage_path('app/thumbnail');
            $img = Image::make($image->path());
            $img->resize(1000, 1000, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$input['imagename']);

            $destinationPath = storage_path('app/users');
            $image->move($destinationPath, 'app/users/'.$input['imagename']);

            $file = new Filesystem();
            $file->cleanDirectory(storage_path('app/thumbnail'));
        }

        $user = $request->all();

        //dd($user);

        User::create($user);
        return redirect('/users');
    }
}
