<?php

namespace App\Http\Controllers;

use App\Assets;
use App\buildings;
use App\Expense;
use App\Vendors;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class AssetsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assets =Assets::all();

        return view('assets.index', compact('assets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $buildings = buildings::orderby('building_name','DESC')->get();
        $vendors = Vendors::orderby('vendor_name','ASC')->get();
        return view('assets.addasset', compact('buildings','vendors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $expense = $request->all();
        $expense = Assets::create($expense);

        Alert::toast('Asset Created!', 'success');
        return redirect('/asset/'.$expense->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Assets  $assets
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $asset = Assets::findorfail($id);

        //dd($asset);

        return view('assets.detail', compact('asset'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Assets  $assets
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $asset = Assets::findorfail($id);
        $buildings = buildings::orderby('building_name','DESC')->get();
        $vendors = Vendors::orderby('vendor_name','ASC')->get();

        //dd($asset);

        return view('assets.editasset', compact('asset','buildings','vendors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Assets  $assets
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //dd($request->id);
        $asset = Assets::findorfail($request->id);
        //$asset = $request->all();
        $asset->update($request->all());

        Alert::toast('Asset Updated!', 'success');
        return redirect('/asset/' . $request->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Assets  $assets
     * @return \Illuminate\Http\Response
     */
    public function destroy(Assets $assets)
    {
        //
    }

    public function label($id)
    {
        //Prints QR tag for asset
        $asset = Assets::findorfail($id);
        return view('assets.label',compact('asset'));
    }
}
