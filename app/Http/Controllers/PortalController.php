<?php

namespace App\Http\Controllers;

use App\Exports\ShippingExport;
use App\Inventory;
use App\Shipments;
use App\States;
use Illuminate\Http\Request;
use Excel;
use RealRashid\SweetAlert\Facades\Alert;

class PortalController extends Controller
{
    public function ship ($id)
    {
        $item = Inventory::where('item_id', '=', $id)->first();
        $states = States::all();
        return view('portal.ship', compact('item','states'));
    }

    public function new_ship (Request $request)
    {
        $newShipment = $request->except('complete');
        $newShipment = Shipments::create($newShipment);  //Create the record

        Alert::toast('Shipment Created!', 'success');

        return redirect('/home');
    }

    public function downloadCSV ()
    {
        //$line = Shipments::where('id', 1)->first()->toArray();
        //dd($line);

        return Excel::download(new ShippingExport(), 'shipping.csv');
    }
}
