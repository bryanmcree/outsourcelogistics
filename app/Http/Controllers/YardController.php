<?php

namespace App\Http\Controllers;

use App\GateControl;
use App\History;
use App\News;
use App\PowerBill;
use App\Yard;
use App\YardTransaction;
use App\YardTransfer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class YardController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    public function test()
    {
        $yard_slots = Yard::all();

        $history = new History();
        $history->action = 'Open Yard App';
        $history->user_id = Auth::user()->email;
        $history->account_type = Auth::user()->account_type;
        $history->search_string = 'User has accessed the yard app.';
        $history->user_ip =  request()->ip();
        $history->save();

        return view('yard.test', compact('yard_slots'));
    }

    public function dock_feed()
    {
        //Get the number of dock rows
        $docks = Yard::where('location_type','=','Dock')->get();
        $half_records =  $docks->count() / 2;

        if(fmod($half_records, 1) == 0.00)
        {
            //Whole number go with it.
            $half_record = $half_records;
        }else{
            //fraction add one to it.
            $half_record = $half_records + .5;
        }

        $first_row = Yard::where('location_type','=','Dock')->skip(0)->take($half_record)->get();
        $second_row = Yard::where('location_type','=','Dock')->skip($half_record)->take($half_record)->get();
        //END of Dock rows


        $yard = Yard::where('location_type','=','Yard')->get();
        $yard_half_records =  $yard->count() / 6.6;

        $yard_first_row = Yard::where('location_type','=','Yard')->skip(0)->take($yard_half_records)->get();
        $yard_second_row = Yard::where('location_type','=','Yard')->skip($yard_half_records)->take($yard_half_records)->get();
        $yard_third_row = Yard::where('location_type','=','Yard')->skip((floor($yard_half_records) * 2))->take($yard_half_records)->get();
        $yard_forth_row = Yard::where('location_type','=','Yard')->skip((floor($yard_half_records) * 3))->take($yard_half_records)->get();
        $yard_fifth_row = Yard::where('location_type','=','Yard')->skip((floor($yard_half_records) * 4))->take($yard_half_records)->get();
        $yard_sixth_row = Yard::where('location_type','=','Yard')->skip((floor($yard_half_records) * 5))->take($yard_half_records)->get();
        $yard_seventh_row = Yard::where('location_type','=','Yard')->skip((floor($yard_half_records) * 6))->take($yard_half_records)->get();
        //dd($yard_third_row);

        return view('yard.yard_feed', compact('first_row','second_row','yard_first_row','yard_second_row',
            'yard_third_row','yard_forth_row','yard_fifth_row','yard_sixth_row','yard_seventh_row'));
    }

    public function yard_feed()
    {
        $docks = Yard::where('location_type','=','Yard')->get();
        $half_records =  $docks->count() / 2;

        if(fmod($half_records, 1) == 0.00)
        {
            //Whole number go with it.
            $half_record = $half_records;
        }else{
            //fraction add one to it.
            $half_record = $half_records + .5;
        }

        $first_row = Yard::where('location_type','=','Dock')->skip(0)->take($half_record)->get();
        $second_row = Yard::where('location_type','=','Dock')->skip($half_record)->take($half_record)->get();

        return view('yard.yard_feed', compact('first_row','second_row'));
    }

    public function yard_detail(Request $request, $id)
    {
        $location = Yard::where('id','=', $id)
            ->with('YardTrans')
            ->get();
        //dd($location);
        return response()->json($location);
    }

    public function ingate(Request $request)
    {
        $newData = $request->all();

        $newData2 = YardTransaction::create($newData);

        //Lets update the yard dashboard
        $yard = Yard::where('id','=', $request->location_id)->first();

        if($yard) {
            if($newData['loaded'] == 'Empty')
            {
                $yard->location_status = 'Complete'; //empty trailer/container
            }else{
                $yard->location_status = 'Occupied';
            }
            $yard->trailer_type = $newData['trailer_type'];
            $yard->current_id = $newData2->id;
            $yard->save();
        }

        if($request->open_gate == 'Yes'){
            $gate = new GateControl();
            $gate->controller_id = 1;
            $gate->controller_command = "Open Driver Gate";
            $gate->created_by = $request->created_by;
            $gate->save();
        }

        $history = new History();
        $history->action = 'Yard - Ingate';
        $history->user_id = Auth::user()->email;
        $history->account_type = Auth::user()->account_type;
        $history->search_string = 'User ingated on the yard';
        $history->user_ip =  request()->ip();
        $history->save();

        Alert::toast('Ingate Complete', 'success');
        return redirect('/yard');
    }

    public function outgate(Request $request)
    {
        $newData = $request->all();
        $newData2 = YardTransaction::create($newData);

        //Lets update the yard dashboard
        $yard = Yard::where('id','=', $request->location_id)->first();
        if($yard) {
            $yard->location_status = 'Empty';
            $yard->current_id = Null; //Nothing left there so clear it out.
            $yard->save();
        }

        if($request->open_gate == 'Yes'){
            $gate = new GateControl();
            $gate->controller_id = 1;
            $gate->controller_command = "Open Driver Gate";
            $gate->created_by = $request->created_by;
            $gate->save();
        }

        $history = new History();
        $history->action = 'Yard - Outgate';
        $history->user_id = Auth::user()->email;
        $history->account_type = Auth::user()->account_type;
        $history->search_string = 'User outgated on the yard';
        $history->user_ip =  request()->ip();
        $history->save();

        Alert::toast('Outgate Complete', 'success');
        return redirect('/yard');
    }

    public function SearchYard(Request $request)
    {

        $yardSearch = YardTransaction::where('current_status', 'LIKE', '%' . $request->yardsearch . '%')
            ->orWhere('trailer_type', 'LIKE', '%' . $request->yardsearch . '%')
            ->orWhere('trailer_number', 'LIKE', '%' . $request->yardsearch . '%')
            ->orWhere('driver_name', 'LIKE', '%' . $request->yardsearch . '%')
            ->orWhere('driver_phone', 'LIKE', '%' . $request->yardsearch . '%')
            ->orWhere('pickup_number', 'LIKE', '%' . $request->yardsearch . '%')
            ->orWhere('customer_name', 'LIKE', '%' . $request->yardsearch . '%')
            ->orderby('created_at','DESC')
            ->paginate(50);

        $history = new History();
        $history->action = 'Yard - Search';
        $history->user_id = Auth::user()->email;
        $history->account_type = Auth::user()->account_type;
        $history->search_string = $request->yardsearch;
        $history->user_ip =  request()->ip();
        $history->save();

        return view('yard.search', compact('yardSearch'));

    }

    public function unloaded(Request $request)
    {
        $CreateTransfer = $request->CreateTransfer;
        $MovePriority = $request->priority;

        if($CreateTransfer == 'Yes'){
            $request->offsetSet('current_status', 'Unloaded / Transfer');
        }

        $newData = $request->except('CreateTransfer','priority');


        $newData2 = YardTransaction::create($newData);

        //Lets update the yard dashboard
        $yard = Yard::where('id','=', $request->location_id)->first();


        if($CreateTransfer == 'Yes'){
            $yardloc = Yard::where('location_status','=','Empty')
                ->where('location_type','=','Yard')
                ->inRandomOrder()
                ->first();

            //dd($yardloc);

            if($yard) {
                $yard->location_status = 'Transfer';
                $yard->current_id = $newData2->id;
                $yard->save();
            }

            if($yardloc) {
                $yardloc->location_status = 'Transfer';
                $yardloc->save();
            }

            //Add Transfer Request

            $transfer = new YardTransfer();
            $transfer->current_location_id = $request->location_id;
            $transfer->new_location_id = $yardloc->id;
            $transfer->detail_id = $yard->current_id;
            $transfer->priority = $MovePriority;
            $transfer->move_status = "Pending";
            $transfer->created_by = $request->created_by;
            $transfer->save();

            Alert::toast('Marked as unloaded and Submitted Transfer', 'success');

        }else{

            if($yard) {
                $yard->location_status = 'Complete';
                $yard->current_id = $newData2->id;
                $yard->save();
            }

            Alert::toast('Marked as unloaded and complete', 'success');
        }

        $history = new History();
        $history->action = 'Yard - Unloaded';
        $history->user_id = Auth::user()->email;
        $history->account_type = Auth::user()->account_type;
        $history->search_string = 'User unloaded on yard';
        $history->user_ip =  request()->ip();
        $history->save();

        return redirect('/yard');
    }

    public function switching()
    {
        //Jockey dashboard for tablet
        return view('yard.jockey_dashboard');

    }

    public function switchFeed()
    {
        $transfer = YardTransfer::where('move_status','=','Pending')
            ->orderby('priority','asc')
            ->orderby('created_at','asc')
            ->get();
        //dd($transfer);

        $alreadyAsisgned = YardTransfer::where('move_status','=','Assigned')
            ->where('assigned_to','=', Auth::user()->id)
            ->first();

        return view('yard.jockey_feed',compact('transfer','alreadyAsisgned'));
    }

    public function RequestAssignment($id)
    {
        $assignment = YardTransfer::findorfail($id);

        //Mark Transfer as Assigned
        if($assignment)
        {
            $assignment->move_status = "Assigned";
            $assignment->assigned_to = Auth::user()->id;
            $assignment->assignment_accepted_time = Carbon::now();
            $assignment->save();
        }

        return view('yard.jockey_accept',compact('assignment'));

    }

    public function CompleteAssignment($id)
    {
        //dd("Hello made it here!");
        $assignment = YardTransfer::findorfail($id);


        //Let's update the assignment
        if($assignment)
        {
            $assignment->move_status = "Complete";
            $assignment->assigned_to = Auth::user()->id;
            $assignment->completed_time = Carbon::now();
            $assignment->save();

            //Create a yard transaction have to do this first to link everything together
            $newTrans = new YardTransaction();
            $newTrans->location_id = $assignment->new_location_id;
            $newTrans->current_status = "On the yard";
            $newTrans->trailer_type = $assignment->TransDetail->trailer_type;
            $newTrans->trailer_number = $assignment->TransDetail->trailer_number;
            $newTrans->loaded = $assignment->TransDetail->loaded;
            $newTrans->driver_name = $assignment->TransDetail->driver_name;
            $newTrans->driver_phone = $assignment->TransDetail->driver_phone;
            $newTrans->pickup_number = $assignment->TransDetail->pickup_number;
            $newTrans->trailer_owner = $assignment->TransDetail->trailer_owner;
            $newTrans->customer_name = $assignment->TransDetail->customer_name;
            $newTrans->open_gate = 'No';
            $newTrans->created_by = $assignment->assigned_to;
            $newTrans->save();

            //Remove current location status

            $yard = Yard::findorfail($assignment->current_location_id);
            $yard->location_status = "Empty";
            $yard->current_id = NULL;
            $yard->trailer_type = NULL;
            $yard->save();

            //Place the container where it was transfered
            $yardNew = Yard::findorfail($assignment->new_location_id);
            $yardNew->location_status = "Occupied";
            $yardNew->current_id = $newTrans->id;
            $yardNew->trailer_type = $assignment->TransDetail->trailer_type;
            $yardNew->save();
        }
        Alert::toast('Move Completed', 'success');
        return redirect('/yard/switch');

    }

    public function AbandonAssignment($id)
    {
        $assignment = YardTransfer::findorfail($id);
        dd($assignment);
    }

}

