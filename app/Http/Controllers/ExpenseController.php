<?php

namespace App\Http\Controllers;

use App\buildings;
use App\Expense;
use App\ExpenseItems;
use App\History;
use App\PTO;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;
use Webpatser\Uuid\Uuid;
use Image;

class ExpenseController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    public function index()
    {
        $pendingRequest = Expense::wherenull('complete')
            ->where('employee_id', Auth::user()->id)
            ->orderby('created_at','desc')
            ->get();

        $completedRequest = Expense::wherenotnull('complete')
            ->where('employee_id', Auth::user()->id)
            ->orderby('created_at','desc')
            ->get();

        $grand_total = Expense::wherenotnull('complete')
            ->where('expense.employee_id', Auth::user()->id)
            ->selectRaw('sum(amount) as grand_total')
            ->join('expense_items','expense.id','=','expense_items.expense_id')
            ->first();

        $grand_total_pending = Expense::wherenull('complete')
            ->where('expense.employee_id', Auth::user()->id)
            ->wherenull('expense_items.deleted_at')
            ->selectRaw('sum(amount) as grand_total')
            ->join('expense_items','expense.id','=','expense_items.expense_id')
            ->first();
        //dd($grand_total);

        $history = new History();
        $history->action = 'Viewed Expenses';
        $history->user_id = Auth::user()->email;
        $history->account_type = Auth::user()->account_type;
        $history->search_string = 'User has viewed expenses.';
        $history->user_ip =  request()->ip();
        $history->save();

        return view('expense.index', compact('pendingRequest','completedRequest','grand_total',
        'grand_total_pending'));
    }

    public function create_expense(Request $request)
    {
        $expense = $request->all();
        $expense = Expense::create($expense);

        $history = new History();
        $history->action = 'Created an Expense';
        $history->user_id = Auth::user()->email;
        $history->account_type = Auth::user()->account_type;
        $history->search_string = 'User has created a new expense.';
        $history->user_ip =  request()->ip();
        $history->save();

        Alert::toast('Expense Report Created!', 'success');
        return redirect('/expense/'. $expense->id);
    }

    public function detail($id)
    {
        $expense = Expense::find($id);
        $buildings = buildings::orderby('building_name','DESC')->get();

        $history = new History();
        $history->action = 'View Expense Detail';
        $history->user_id = Auth::user()->email;
        $history->account_type = Auth::user()->account_type;
        $history->search_string = 'User has viewed expense detail.';
        $history->user_ip =  request()->ip();
        $history->save();

        return view('expense.detail', compact('expense','buildings'));
    }

    public function create_item(Request $request)
    {

        if(!is_null($request->file_name)){ //Prevents error if no receipt is attached
            $request->offsetSet('extension', $request->file_name->getClientOriginalExtension());
            $request->offsetSet('old_filename', $request->file_name->getClientOriginalName());
            $request->offsetSet('new_filename', Uuid::generate(4) .'.'.$request->extension);

            $image = $request->file('file_name');
            $input['imagename'] = $request->new_filename;

            $destinationPath = storage_path('app/thumbnail');
            $img = Image::make($image->path());
            $img->resize(1000, 1000, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$input['imagename']);

            $destinationPath = storage_path('app/receipts');
            $image->move($destinationPath, 'app/receipts/'.$input['imagename']);

            $file = new Filesystem();
            $file->cleanDirectory(storage_path('app/thumbnail'));
        }

        $item = $request->all();


        $item = ExpenseItems::create($item);

        $history = new History();
        $history->action = 'Added Expense Item';
        $history->user_id = Auth::user()->email;
        $history->account_type = Auth::user()->account_type;
        $history->search_string = 'User has added expense item.';
        $history->user_ip =  request()->ip();
        $history->save();

        Alert::toast('Expense Report Created!', 'success');
        return redirect('/expense/'. $item->expense_id);
    }

    public function print_expense_report($id)
    {
        $expense = Expense::find($id);

        $pdf = PDF::loadView('expense.reports.print_expense_report', compact('expense'));
        //return $pdf->download('Expense_Report_' . Carbon::now() . '.pdf');
        return view('expense.reports.print_expense_report', compact('expense'));
    }

    public function deleteExpense(Request $request)
    {
        $remove_item = Expense::where('id', $request->id)->delete();
        Alert::toast('Expense Deleted', 'success');
        return redirect('/expense');
    }

    public function markComplete(Request $request)
    {
        $completed = Expense::where('id', $request->id)
            ->first();
        $completed->complete = 'Yes';
        $completed->save();

        //Notify Accounting of new expense report
        Mail::send([], [], function ($message)  {
            $message->to('bryanmcree@outsourcelogistics.com')
                //->CC('joloresduncan@outsourcelogistics.com')
                //->CC('maxgowan@outsourcelogistics.com')
                ->subject('New Expense Report')
                ->from('no-reply@outsourcelogistics.com', 'Expense Report')
                ->priority(1)
                ->setBody('<h1 style="color:red">A new expense report has been submitted</h1>'
                    , 'text/html'); // for HTML rich messages.
        });

        Alert::toast('Expense Completed', 'success');
        return redirect('/expense');
    }

    public function deleteItem(Request $request)
    {
        $remove_item = ExpenseItems::where('id', $request->id)->first();
        $expense_id = $remove_item->expense_id;
        $remove_item->delete();
        Alert::toast('Expense Deleted', 'success');
        return redirect('/expense/' . $expense_id);
    }
}
