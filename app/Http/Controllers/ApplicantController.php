<?php

namespace App\Http\Controllers;

use App\Clients;
use App\Expense;
use App\Role_User;
use App\Roles;
use App\States;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;

class ApplicantController extends Controller
{
    public function index()
    {
        return view('applicant.index');
    }

    public function register()
    {
        $states = States::all();
        return view('applicant.register', compact('states'));
    }

    public function newRegistration(Request $request)
    {
        $request->offsetSet('password', Hash::make($request->password));  //HASH Password
        $request->offsetSet('ssn', Crypt::encryptString($request->ssn));  //Encrypt SSN
        $NewApp = $request->all();  //collect all other fields
        $NewApp = User::create($NewApp);  //Create the record

        //Set Security Roles for Applicant
        $applicantRole = Roles::where('name','Applicant')->first();  //Find the applicant role
        $addRole = new Role_User();
        $addRole->user_id = $NewApp->id;
        $addRole->roles_id = $applicantRole->id;
        $addRole->save();

        Auth::loginUsingId($NewApp->id);

        Alert::toast('Account Created!', 'success');
        return redirect('/home');
    }
}
