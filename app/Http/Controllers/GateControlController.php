<?php

namespace App\Http\Controllers;

use App\GateControl;
use App\History;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Auth;

class GateControlController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','verified'],['except' => 'DriveGateURL','DriveGateDelete1']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('gate.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GateControl  $gateControl
     * @return \Illuminate\Http\Response
     */
    public function show(GateControl $gateControl)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GateControl  $gateControl
     * @return \Illuminate\Http\Response
     */
    public function edit(GateControl $gateControl)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GateControl  $gateControl
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GateControl $gateControl)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GateControl  $gateControl
     * @return \Illuminate\Http\Response
     */
    public function destroy(GateControl $gateControl)
    {
        //
    }

    public function DriveGate(Request $request)
    {
        $cmd_from = $request->cmd_from;
        $command = $request->except('cmd_from');
        $command = GateControl::create($command);

        $history = new History();
        $history->action = 'Open Gate';
        $history->user_id = Auth::user()->email;
        $history->account_type = Auth::user()->account_type;
        $history->search_string = $command['controller_command'];
        $history->user_ip = request()->ip();
        $history->save();

        Alert::toast('Request Complete!', 'success');

        if($cmd_from =='yard')
        {
            return redirect('/yard');
        }else{
            return redirect('/gate');
        }



    }

    public function DriveGateURL()
    {

        $getRequest = GateControl::select('controller_command')->where('controller_id','=',1)->first();

        if(is_null($getRequest)){
            $data = [
                'controller_command' => "No Command"
            ];
        }else{
            $data = [
                'controller_command' => $getRequest['controller_command']
            ];
        }
        GateControl::wherenull('deleted_at')->delete();
        return $data;

    }

    public function DriveGateDelete1()
    {




    }
}
