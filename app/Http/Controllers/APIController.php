<?php

namespace App\Http\Controllers;

use App\Assets;
use Illuminate\Http\Request;

class APIController extends Controller
{
    public function getAllassets()
    {
        $assets = Assets::get()->toJson(JSON_PRETTY_PRINT);
        header('Content-Type: application/json');
        return response($assets, 200);
    }

}
