<?php

namespace App\Http\Controllers;

use App\History;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BarcodeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        return view('barcode.index');
    }

    public function bigsingle()
    {
        return view('barcode.bigsingle');
    }

    public function print(Request $request)
    {
        $label = $request->all();
        $barcode = $label['barcode'];
        //dd($label['barcode']);
        $pdf = PDF::loadView('barcode.reports.8by11', compact('barcode'));
        //return $pdf->download('Expense_Report_' . Carbon::now() . '.pdf');

        $history = new History();
        $history->action = 'Print Barcode';
        $history->user_id = Auth::user()->email;
        $history->account_type = Auth::user()->account_type;
        $history->search_string = 'User has printed a barcode.';
        $history->user_ip =  request()->ip();
        $history->save();

        return view('barcode.reports.8by11', compact('barcode'));
    }

    public function print2(Request $request)
    {
        $label = $request->all();
        //dd($label);
        $barcode_left = $label['barcode_left'];
        $barcode_right = $label['barcode_right'];

        $history = new History();
        $history->action = 'Print Barcode';
        $history->user_id = Auth::user()->email;
        $history->account_type = Auth::user()->account_type;
        $history->search_string = 'User has printed a barcode.';
        $history->user_ip =  request()->ip();
        $history->save();

        if($label['rowside'] == "Right")
        {
            return view('barcode.reports.rightdualcode', compact('barcode_right', 'barcode_left'));
        }else{
            return view('barcode.reports.dualcode', compact('barcode_right', 'barcode_left'));
        }

    }

    public function dualcode()
    {
        return view('barcode.dualcodes');
    }
}
