<?php

namespace App\Http\Controllers;

use App\buildings;
use Illuminate\Http\Request;

class BuildingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\buildings  $buildings
     * @return \Illuminate\Http\Response
     */
    public function show(buildings $buildings)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\buildings  $buildings
     * @return \Illuminate\Http\Response
     */
    public function edit(buildings $buildings)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\buildings  $buildings
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, buildings $buildings)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\buildings  $buildings
     * @return \Illuminate\Http\Response
     */
    public function destroy(buildings $buildings)
    {
        //
    }
}
