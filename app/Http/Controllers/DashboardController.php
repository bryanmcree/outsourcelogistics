<?php

namespace App\Http\Controllers;

use App\History;
use App\OslDashboard;
use App\OSLDashboard_bin;
use App\OslDashboard_exec;
use App\OSLDashboard_time;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class DashboardController extends Controller
{

    public function osl()
    {
        $dashboard = OslDashboard::whereDate('created_at', Carbon::today())->get();
        $binuseage = OSLDashboard_bin::whereDate('created_at', Carbon::today())->first();
        $execError = OslDashboard_exec::all();
        $timeCustomer = OSLDashboard_time::all();

        //dd($dashboard);
        //if($timeCustomer)
        //{
        //    Mail::send([], [], function ($message)  {
        //        $message->to('bryanmcree@outsourcelogistics.com')
        //            ->CC('customerservice@outsourcelogistics.com')
        //            ->CC('maxgowan@outsourcelogistics.com')
        //            ->subject('Time Sensitive Customer Entered')
        //            ->from('no-reply@outsourcelogistics.com', 'Time Sensitive')
        //            ->priority(1)
        //            ->setBody('<h1 style="color:red">A time sensitive customers transaction has been submitted with paperwork.</h1>'
        //                , 'text/html'); // for HTML rich messages.
        //    });
        //}




        if($dashboard->isEmpty()) //If there is no data for today, take user to error page.
        {


            return view('dashboards.error');
        }else{
            //$error_check = OslDashboard::whereDate('created_at', Carbon::today())
            //    ->orderby('created_at')
            //    ->first();

            //If there is an error in the last import send off some email.
            //if($error_check['QueStatus'] == 9) //Disabled
            //{
            //    Mail::send([], [], function ($message)  {
            //        $message->to('bryanmcree@outsourcelogistics.com')
            //            ->CC('joloresduncan@outsourcelogistics.com')
            //            ->CC('maxgowan@outsourcelogistics.com')
            //            ->subject('Camelot Data Integration - FAILURE TEST----')
            //            ->from('no-reply@outsourcelogistics.com', 'Flat File Failure')
            //            ->priority(1)
            //            ->setBody('<h1 style="color:red">Camelot Data Integration Failure</h1>'
            //                , 'text/html'); // for HTML rich messages.
            //    });
            //}



            return view('dashboards.osl', compact('dashboard', 'binuseage','execError','timeCustomer'));
        }

    }

    public function error()
    {
        return view('dashboards.error');
    }
}
