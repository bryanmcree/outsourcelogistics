<?php

namespace App\Http\Controllers;

use App\ContainerNotes;
use Illuminate\Http\Request;

class ContainerNotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContainerNotes  $containerNotes
     * @return \Illuminate\Http\Response
     */
    public function show(ContainerNotes $containerNotes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContainerNotes  $containerNotes
     * @return \Illuminate\Http\Response
     */
    public function edit(ContainerNotes $containerNotes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContainerNotes  $containerNotes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContainerNotes $containerNotes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContainerNotes  $containerNotes
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContainerNotes $containerNotes)
    {
        //
    }
}
