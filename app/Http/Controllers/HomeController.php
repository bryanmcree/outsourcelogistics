<?php

namespace App\Http\Controllers;

use App\Inventory;
use App\Shipments;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //Employee Search
        $emps = User::select('first_name','last_name','email','work_phone','work_ext')->get();

        //Portal Inventory
        $items = Inventory::all();

        $pendingShipments = Shipments::wherenull('order_status')->get();
        //dd($pendingShipments);

        if(Auth::user()->account_type == 'Customer'){
            return view('portal.index', compact('items'));
        }elseif(Auth::user()->account_type == 'Employee'){
            return view('home', compact('emps'));
        }elseif(Auth::user()->account_type == 'Disabled'){
            return view('error_pages.disabled', compact('emps'));
        }elseif(is_null(Auth::user()->account_type)){
            return view('error_pages.validating_account', compact('emps'));
        }

    }
}
