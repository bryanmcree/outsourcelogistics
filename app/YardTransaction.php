<?php

namespace App;

use App\Traits\Observable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class YardTransaction extends Model
{
    use SoftDeletes;
    use Observable;

    public static function logSubject(Model $model): string
    {
        return sprintf( "User [id:%d] %s/%s",
            $model->id, $model->name, $model->email
        );
    }
    protected $guarded = [];
    protected $table = 'yard_transaction';

    public function employee(){
        return $this->hasOne('\App\User','id','created_by');
    }


    public function YardSlot(){
        return $this->hasOne('\App\Yard','id','location_id');
    }
}
