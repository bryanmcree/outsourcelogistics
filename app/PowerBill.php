<?php

namespace App;

use App\Traits\Observable;
use Illuminate\Database\Eloquent\Model;

class PowerBill extends Model
{
    use Observable;

    public static function logSubject(Model $model): string
    {
        return sprintf( "User [id:%d] %s/%s",
            $model->id, $model->name, $model->email
        );
    }
    protected $dates = ['bill_date'];
    protected $guarded = [];

}
